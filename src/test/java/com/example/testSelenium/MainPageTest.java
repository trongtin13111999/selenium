package com.example.testSelenium;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.selenide.AllureSelenide;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.junit.jupiter.api.*;

import org.openqa.selenium.*;

import static com.codeborne.selenide.Selenide.$x;
import static org.junit.jupiter.api.Assertions.*;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.lang.*;

public class MainPageTest {
    WebDriver driver;
    MainPage mainPage = new MainPage();
    Map<String, Object[]> resultsTest;
    HSSFWorkbook workbook;
    HSSFSheet sheet;
    DateTimeFormatter dtf = DateTimeFormatter.BASIC_ISO_DATE.ofPattern("dd/MM/yyyy HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
    String error = "";

    @BeforeAll
    public static void setUpAll() {
        Configuration.browserSize = "1536x864";
        SelenideLogger.addListener("allure", new AllureSelenide());
    }

    @BeforeClass
    public void setUpBeforeClass() {
        WebDriverManager.chromedriver().setup();
        Configuration.startMaximized = true;
        workbook = new HSSFWorkbook();
        sheet = workbook.createSheet("Result test");
        resultsTest = new LinkedHashMap<String, Object[]>();
        resultsTest.put("1", new Object[]{"Test step No.", "Action", "Test case description", "Test Data", "Expected Result", "Actual Result", "Date", "Result"});
        try {
        } catch (Exception e) {
            throw new IllformedLocaleException("Can't start");
        }
    }

    @BeforeEach
    void setUp() {
        open("https://www.thegioididong.com/");
    }

    @Test(priority = 1)
    public void setUpBefore() {
        System.out.println("Hello");
        System.out.println(dtf.format(now));
    }
    // Thanh toán tiền mạng FPT

    @Test(priority = 2)
    public void payTheBillFPT() {
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@href=\"/tien-ich\"]").click();
            $x("//*[@href=\"/tien-ich/thanh-toan-internet-fpt\"]").click();
            $x("//*[@href=\"javascript:viewDetail()\"]").hover();
            $x("//*[@name=\"txtUserCode\"]").sendKeys("SGH425958");
            $x("//*[@name=\"strPhoneNumber\"]").sendKeys("0795421952");
            $x("//*[@class='checkpay']").click();
            resultsTest.put("2", new Object[]{
                1d,
                "Test trường hợp thanh toán hóa đơn tiền mạng thành công",
                "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Trả góp, điện nước.\n3- Chọn Đóng tiền Net FPT\n4- Điền mã khách hàng và số điện thoại liên hệ\n5- Click vào Xem hóa đơn và thanh toán",
                "Mã khách hàng: SGH425958\nSố điện thoại liên hệ: 0795421952",
                "Thông báo Lỗi giao dịch: tài khoản không nợ cước",
                "Thông báo Lỗi giao dịch: tài khoản không nợ cước",
                dtf.format(now),
                "Pass",
            });
        } catch (AssertionError e) {
            resultsTest.put("2", new Object[]{
                1d,
                "Test trường hợp thanh toán hóa đơn tiền mạng thành công",
                "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Trả góp, điện nước.\n3- Chọn Đóng tiền Net FPT\n4- Điền mã khách hàng và số điện thoại liên hệ\n5- Click vào Xem hóa đơn và thanh toán",
                "Mã khách hàng: SGH425958\nSố điện thoại liên hệ: 0795421952",
                "Thông báo Lỗi giao dịch: tài khoản không nợ cước",
                "Không hiển thị thông báo Lỗi giao dịch: tài khoản không nợ cước",
                dtf.format(now),
                "Fail",
            });
            Assert.assertTrue(false);
        }
    }

    @Test(priority = 3)
    public void payTheBillFPTNotUserCode() {
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@href=\"/tien-ich\"]").click();
            $x("//*[@href=\"/tien-ich/thanh-toan-internet-fpt\"]").click();
            $x("//*[@href=\"javascript:viewDetail()\"]").hover();
//            $x("//*[@name=\"txtUserCode\"]").sendKeys("SGH425958");
            $x("//*[@name=\"strPhoneNumber\"]").sendKeys("0795421952");
            $x("//*[@class='checkpay']").click();
            resultsTest.put("3", new Object[]{
                    2d,
                    "Test trường hợp thanh toán hóa đơn tiền mạng không điền Mã khách hàng",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Trả góp, điện nước.\n3- Chọn Đóng tiền Net FPT\n4- Điền số điện thoại liên hệ (Không điền mã khách hàng)\n5- Click vào Xem hóa đơn và thanh toán",
                    "Mã khách hàng: \nSố điện thoại liên hệ: 0795421952",
                    "Thông báo Nhập mã khách hàng",
                    "Thông báo Nhập mã khách hàng",
                    dtf.format(now),
                    "Pass",
            });
        } catch (AssertionError e) {
            resultsTest.put("3", new Object[]{
                    2d,
                    "Test trường hợp thanh toán hóa đơn tiền mạng không điền Mã khách hàng",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Trả góp, điện nước.\n3- Chọn Đóng tiền Net FPT\n4- Điền số điện thoại liên hệ (Không điền mã khách hàng)\n5- Click vào Xem hóa đơn và thanh toán",
                    "Mã khách hàng: \nSố điện thoại liên hệ: 0795421952",
                    "Thông báo Nhập mã khách hàng",
                    "Không hiển thị thông báo Nhập mã khách hàng",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }

    @Test(priority = 4)
    public void payTheBillFPTNotPhone() {
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@href=\"/tien-ich\"]").click();
            $x("//*[@href=\"/tien-ich/thanh-toan-internet-fpt\"]").click();
            $x("//*[@href=\"javascript:viewDetail()\"]").hover();
            $x("//*[@name=\"txtUserCode\"]").sendKeys("SGH425958");
            // $x("//*[@name=\"strPhoneNumber\"]").sendKeys("0795421952");
            $x("//*[@class='checkpay']").click();
            resultsTest.put("4", new Object[]{
                    3d,
                    "Test trường hợp thanh toán hóa đơn tiền mạng không điền số điện thoại",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Trả góp, điện nước.\n3- Chọn Đóng tiền Net FPT\n4- Điền mã số khách hàng (Không điền số điện thoại)\n5- Click vào Xem hóa đơn và thanh toán",
                    "Mã khách hàng: SGH425958\nSố điện thoại liên hệ:",
                    "Thông báo Vui lòng nhập điện thoại",
                    "Thông báo Vui lòng nhập điện thoại",
                    dtf.format(now),
                    "Pass",
            });
        } catch (AssertionError e) {
            resultsTest.put("4", new Object[]{
                    3d,
                    "Test trường hợp thanh toán hóa đơn tiền mạng không điền số điện thoại",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Trả góp, điện nước.\n3- Chọn Đóng tiền Net FPT\n4- Điền mã số khách hàng (Không điền số điện thoại)\n5- Click vào Xem hóa đơn và thanh toán",
                    "Mã khách hàng: SGH425958\nSố điện thoại liên hệ:",
                    "Thông báo Vui lòng nhập điện thoại",
                    "Không hiển thị thông báo Vui lòng nhập điện thoại",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }

    @Test(priority = 5)
    public void payTheBillFPTNotFill() {
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@href=\"/tien-ich\"]").click();
            $x("//*[@href=\"/tien-ich/thanh-toan-internet-fpt\"]").click();
            $x("//*[@href=\"javascript:viewDetail()\"]").hover();
            // $x("//*[@name=\"txtUserCode\"]").sendKeys("SGH425958");
            // $x("//*[@name=\"strPhoneNumber\"]").sendKeys("0795421952");
            $x("//*[@class='checkpay']").click();
            resultsTest.put("5", new Object[]{
                    4d,
                    "Test trường hợp thanh toán hóa đơn tiền mạng không điền thông tin",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Trả góp, điện nước.\n3- Chọn Đóng tiền Net FPT\n5- Click vào Xem hóa đơn và thanh toán",
                    "Mã khách hàng: \nSố điện thoại liên hệ:",
                    "Thông báo Nhập mã khách hàng",
                    "Thông báo Nhập mã khách hàng",
                    dtf.format(now),
                    "Pass",
            });
        } catch (AssertionError e) {
            resultsTest.put("5", new Object[]{
                    4d,
                    "Test trường hợp thanh toán hóa đơn tiền mạng không điền thông tin",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Trả góp, điện nước.\n3- Chọn Đóng tiền Net FPT\n5- Click vào Xem hóa đơn và thanh toán",
                    "Mã khách hàng: \nSố điện thoại liên hệ:",
                    "Thông báo Nhập mã khách hàng",
                    "Không hiển thị thông báo Nhập mã khách hàng",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    // Nạp tiền điện thoại
    @Test(priority = 6)
    public void mobileRecharge() {
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@href=\"/sim-so-dep\"]").click();
            $x("//*[@class=\"titlesimcard\"]").hover();
            $x("//*[@id=\"txtPhoneAirtime\"]").sendKeys("0795421952");
            $x("//img[@src='/sim-so-dep/Content/desktop/images/brands/mobi.png']").click();
            $x("//*[@data-value=\"4103122001400\" and @data-price=\"48500\"]").click();
            String captcha = "";
            captcha =  $x("//*[@class=\"imgcaptcha\"]").getText();
            $x("//*[@name=\"captcha\"]").sendKeys(captcha);
            // $x("//*[@id=\"btnAirtimeATM\"]").click();
            resultsTest.put("6", new Object[]{
                    5d,
                    "Test trường hợp nạp tiền điện thoại thành công",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Sim, Thẻ cào.\n3- Scroll xuống chỗ Nạp tiền điện thoại\n5- Điền số điện thoại và chọn các option\n6- Nhập mã captcha\n7- Click vào button chọn Dùng thẻ ATM",
                    "Số điện thoại: 0795421952\nChọn nhà mạng: Mobifone\nChọn số tiền nạp: 50.000đ\n",
                    "Thông báo Nhập mã khách hàng",
                    "Thông báo Nhập mã khách hàng",
                    dtf.format(now),
                    "Pass",
            });
        } catch (AssertionError e) {
            resultsTest.put("6", new Object[]{
                    5d,
                    "Test trường hợp nạp tiền điện thoại thành công",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Sim, Thẻ cào.\n3- Scroll xuống chỗ Nạp tiền điện thoại\n5- Điền số điện thoại và chọn các option\n6- Nhập mã captcha\n7- Click vào button chọn Dùng thẻ ATM",
                    "Số điện thoại: 0795421952\nChọn nhà mạng: Mobifone\nChọn số tiền nạp: 50.000đ\n",
                    "Thông báo Nhập mã khách hàng",
                    "Không hiển thị thông báo Nhập mã khách hàng",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }

    @Test(priority = 7)
    public void mobileRechargeNotListActiveNetwork() {
        error = "";
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@href=\"/sim-so-dep\"]").click();
            $x("//*[@class=\"titlesimcard\"]").hover();
            $x("//*[@id=\"txtPhoneAirtime\"]").sendKeys("0795421952");
            // $x("//img[@src='/sim-so-dep/Content/desktop/images/brands/mobi.png']").click();
            $x("//*[@data-value=\"4103122001400\" and @data-price=\"50000\"]").click();
            // String captcha = "";
            // captcha =  $x("//*[@class=\"imgcaptcha\"]").getText();
            // $x("//*[@name=\"captcha\"]").sendKeys(captcha);
            // $x("//*[@id=\"btnAirtimeATM\"]").click();
            error = $x("//*[@id=\"error\"]").getText();
            if (error != "") {
                resultsTest.put("7", new Object[]{
                    6d,
                    "Test trường hợp nạp tiền điện thoại không chọn nhà mạng",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Sim, Thẻ cào.\n3- Scroll xuống chỗ Nạp tiền điện thoại\n5- Điền số điện thoại và chọn các option (Không chọn nhà mạng)",
                    "Số điện thoại: 0795421952\nChọn nhà mạng: Mobifone\nChọn số tiền nạp: 50.000đ\n",
                    "Thông báo "+error,
                    "Thông báo "+error,
                    dtf.format(now),
                    "Pass",
                });
            }
        } catch (AssertionError e) {
            resultsTest.put("7", new Object[]{
                    6d,
                    "Test trường hợp nạp tiền điện thoại không chọn nhà mạng",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Sim, Thẻ cào.\n3- Scroll xuống chỗ Nạp tiền điện thoại\n5- Điền số điện thoại và chọn các option (Không chọn nhà mạng)",
                    "Số điện thoại: 0795421952\nChọn nhà mạng: Mobifone\nChọn số tiền nạp: 50.000đ\n",
                    "Thông báo "+error,
                    "Không hiển thị thông báo "+error,
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }

    @Test(priority = 8)
    public void mobileRechargeNotPhone() {
        error = "";
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@href=\"/sim-so-dep\"]").click();
            $x("//*[@class=\"titlesimcard\"]").hover();
//            $x("//*[@id=\"txtPhoneAirtime\"]").sendKeys("0795421952");
             $x("//img[@src='/sim-so-dep/Content/desktop/images/brands/mobi.png']").click();
//            $x("//*[@data-value=\"4103122001400\" and @data-price=\"50000\"]").click();
            // String captcha = "";
            // captcha =  $x("//*[@class=\"imgcaptcha\"]").getText();
            // $x("//*[@name=\"captcha\"]").sendKeys(captcha);
            // $x("//*[@id=\"btnAirtimeATM\"]").click();
            error = $x("//*[@id=\"error\"]").getText();
            if (error != "") {
                resultsTest.put("8", new Object[]{
                        7d,
                        "Test trường hợp nạp tiền điện thoại không điền số điện thoại",
                        "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Sim, Thẻ cào.\n3- Scroll xuống chỗ Nạp tiền điện thoại\n5- Không Điền số điện thoại và chọn các option",
                        "Số điện thoại: 0795421952\nChọn nhà mạng: Mobifone\nChọn số tiền nạp: 50.000đ\n",
                        "Thông báo "+error,
                        "Thông báo "+error,
                        dtf.format(now),
                        "Pass",
                });
            }
        } catch (AssertionError e) {
            resultsTest.put("8", new Object[]{
                    7d,
                    "Test trường hợp nạp tiền điện thoại không điền số điện thoại",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Sim, Thẻ cào.\n3- Scroll xuống chỗ Nạp tiền điện thoại\n5- Không Điền số điện thoại và chọn các option",
                    "Số điện thoại: 0795421952\nChọn nhà mạng: Mobifone\nChọn số tiền nạp: 50.000đ\n",
                    "Thông báo "+error,
                    "Không hiển thị thông báo "+error,
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }

    @AfterClass
    public void Finish() {
        System.out.println("Kết thúc");
        Set<String> keyset = resultsTest.keySet();
        int rowNum = 0;
        for (String key : keyset) {
            Row row = sheet.createRow(rowNum++);
            Object[] objArr = resultsTest.get(key);
            int cellNum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellNum++);
                if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {
            FileOutputStream out = new FileOutputStream(new File("ResultTestToExCel.xls"));
            workbook.write(out);
            out.close();
            System.out.println("In file excel thành công!");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
