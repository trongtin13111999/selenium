package com.example.testSelenium;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.selenide.AllureSelenide;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.junit.jupiter.api.*;

import org.openqa.selenium.*;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$x;
import static org.junit.jupiter.api.Assertions.*;

import static com.codeborne.selenide.Selenide.*;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.lang.*;

public class MainPageTestUpdate {
    WebDriver driver;
    MainPage mainPage = new MainPage();
    Map<String, Object[]> resultsTest;
    HSSFWorkbook workbook;
    HSSFSheet sheet;
    DateTimeFormatter dtf = DateTimeFormatter.BASIC_ISO_DATE.ofPattern("dd/MM/yyyy HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
    String error = "";

    @BeforeAll
    public static void setUpAll() {
        Configuration.browserSize = "1536x864";
        SelenideLogger.addListener("allure", new AllureSelenide());
    }

    @BeforeClass
    public void setUpBeforeClass() {
        WebDriverManager.chromedriver().setup();
        Configuration.startMaximized = true;
        workbook = new HSSFWorkbook();
        sheet = workbook.createSheet("Result test");
        resultsTest = new LinkedHashMap<String, Object[]>();
        resultsTest.put("1", new Object[]{"Test step No.", "Action", "Test case description", "Test Data", "Expected Result", "Actual Result", "Date", "Result"});
        try {
        } catch (Exception e) {
            throw new IllformedLocaleException("Can't start");
        }
    }

    @BeforeEach
    void setUp() {
        open("https://www.thegioididong.com/");
    }

    @Test(priority = 1)
    public void setUpBefore() {
        System.out.println("Hello");
        System.out.println(dtf.format(now));
    }
    // Thanh toán tiền mạng FPT
    @Test(priority = 2)
    public void payTheBillFPT() {
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@href=\"/tien-ich\"]").click();
            $x("//*[@href=\"/tien-ich/thanh-toan-internet-fpt\"]").click();
            $x("//*[@href=\"javascript:viewDetail()\"]").hover();
            $x("//*[@name=\"txtUserCode\"]").sendKeys("SGH425958");
            $x("//*[@name=\"strPhoneNumber\"]").sendKeys("0795421952");
            $x("//*[@class='checkpay']").click();
            resultsTest.put("2", new Object[]{
                    1d,
                    "Test trường hợp thanh toán hóa đơn tiền mạng thành công",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Trả góp, điện nước.\n3- Chọn Đóng tiền Net FPT\n4- Điền mã khách hàng và số điện thoại liên hệ\n5- Click vào Xem hóa đơn và thanh toán",
                    "Mã khách hàng: SGH425958\nSố điện thoại liên hệ: 0795421952",
                    "Thông báo Lỗi giao dịch: tài khoản không nợ cước",
                    "Thông báo Lỗi giao dịch: tài khoản không nợ cước",
                    dtf.format(now),
                    "Pass",
            });
        } catch (AssertionError e) {
            resultsTest.put("2", new Object[]{
                    1d,
                    "Test trường hợp thanh toán hóa đơn tiền mạng thành công",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Trả góp, điện nước.\n3- Chọn Đóng tiền Net FPT\n4- Điền mã khách hàng và số điện thoại liên hệ\n5- Click vào Xem hóa đơn và thanh toán",
                    "Mã khách hàng: SGH425958\nSố điện thoại liên hệ: 0795421952",
                    "Thông báo Lỗi giao dịch: tài khoản không nợ cước",
                    "Không hiển thị thông báo Lỗi giao dịch: tài khoản không nợ cước",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }

    @Test(priority = 3)
    public void payTheBillFPTNotUserCode() {
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@href=\"/tien-ich\"]").click();
            $x("//*[@href=\"/tien-ich/thanh-toan-internet-fpt\"]").click();
            $x("//*[@href=\"javascript:viewDetail()\"]").hover();
//            $x("//*[@name=\"txtUserCode\"]").sendKeys("SGH425958");
            $x("//*[@name=\"strPhoneNumber\"]").sendKeys("0795421952");
            $x("//*[@class='checkpay']").click();
            resultsTest.put("3", new Object[]{
                    2d,
                    "Test trường hợp thanh toán hóa đơn tiền mạng không điền Mã khách hàng",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Trả góp, điện nước.\n3- Chọn Đóng tiền Net FPT\n4- Điền số điện thoại liên hệ (Không điền mã khách hàng)\n5- Click vào Xem hóa đơn và thanh toán",
                    "Mã khách hàng: \nSố điện thoại liên hệ: 0795421952",
                    "Thông báo Nhập mã khách hàng",
                    "Thông báo Nhập mã khách hàng",
                    dtf.format(now),
                    "Pass",
            });
        } catch (AssertionError e) {
            resultsTest.put("3", new Object[]{
                    2d,
                    "Test trường hợp thanh toán hóa đơn tiền mạng không điền Mã khách hàng",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Trả góp, điện nước.\n3- Chọn Đóng tiền Net FPT\n4- Điền số điện thoại liên hệ (Không điền mã khách hàng)\n5- Click vào Xem hóa đơn và thanh toán",
                    "Mã khách hàng: \nSố điện thoại liên hệ: 0795421952",
                    "Thông báo Nhập mã khách hàng",
                    "Không hiển thị thông báo Nhập mã khách hàng",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }

    @Test(priority = 4)
    public void payTheBillFPTNotPhone() {
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@href=\"/tien-ich\"]").click();
            $x("//*[@href=\"/tien-ich/thanh-toan-internet-fpt\"]").click();
            $x("//*[@href=\"javascript:viewDetail()\"]").hover();
            $x("//*[@name=\"txtUserCode\"]").sendKeys("SGH425958");
            // $x("//*[@name=\"strPhoneNumber\"]").sendKeys("0795421952");
            $x("//*[@class='checkpay']").click();
            resultsTest.put("4", new Object[]{
                    3d,
                    "Test trường hợp thanh toán hóa đơn tiền mạng không điền số điện thoại",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Trả góp, điện nước.\n3- Chọn Đóng tiền Net FPT\n4- Điền mã số khách hàng (Không điền số điện thoại)\n5- Click vào Xem hóa đơn và thanh toán",
                    "Mã khách hàng: SGH425958\nSố điện thoại liên hệ:",
                    "Thông báo Vui lòng nhập điện thoại",
                    "Thông báo Vui lòng nhập điện thoại",
                    dtf.format(now),
                    "Pass",
            });
        } catch (AssertionError e) {
            resultsTest.put("4", new Object[]{
                    3d,
                    "Test trường hợp thanh toán hóa đơn tiền mạng không điền số điện thoại",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Trả góp, điện nước.\n3- Chọn Đóng tiền Net FPT\n4- Điền mã số khách hàng (Không điền số điện thoại)\n5- Click vào Xem hóa đơn và thanh toán",
                    "Mã khách hàng: SGH425958\nSố điện thoại liên hệ:",
                    "Thông báo Vui lòng nhập điện thoại",
                    "Không hiển thị thông báo Vui lòng nhập điện thoại",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }

    @Test(priority = 5)
    public void payTheBillFPTNotFill() {
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@href=\"/tien-ich\"]").click();
            $x("//*[@href=\"/tien-ich/thanh-toan-internet-fpt\"]").click();
            $x("//*[@href=\"javascript:viewDetail()\"]").hover();
            // $x("//*[@name=\"txtUserCode\"]").sendKeys("SGH425958");
            // $x("//*[@name=\"strPhoneNumber\"]").sendKeys("0795421952");
            $x("//*[@class='checkpay']").click();
            resultsTest.put("5", new Object[]{
                    4d,
                    "Test trường hợp thanh toán hóa đơn tiền mạng không điền thông tin",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Trả góp, điện nước.\n3- Chọn Đóng tiền Net FPT\n5- Click vào Xem hóa đơn và thanh toán",
                    "Mã khách hàng: \nSố điện thoại liên hệ:",
                    "Thông báo Nhập mã khách hàng",
                    "Thông báo Nhập mã khách hàng",
                    dtf.format(now),
                    "Pass",
            });
        } catch (AssertionError e) {
            resultsTest.put("5", new Object[]{
                    4d,
                    "Test trường hợp thanh toán hóa đơn tiền mạng không điền thông tin",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Trả góp, điện nước.\n3- Chọn Đóng tiền Net FPT\n5- Click vào Xem hóa đơn và thanh toán",
                    "Mã khách hàng: \nSố điện thoại liên hệ:",
                    "Thông báo Nhập mã khách hàng",
                    "Không hiển thị thông báo Nhập mã khách hàng",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    // Nạp tiền điện thoại
    @Test(priority = 6)
    public void mobileRecharge() {
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@href=\"/sim-so-dep\"]").click();
            $x("//*[@class=\"titlesimcard\"]").hover();
            $x("//*[@id=\"txtPhoneAirtime\"]").sendKeys("0795421952");
            $x("//img[@src='/sim-so-dep/Content/desktop/images/brands/mobi.png']").click();
            $x("//*[@data-value=\"4103122001400\" and @data-price=\"48500\"]").click();
            String captcha = "";
            captcha =  $x("//*[@class=\"imgcaptcha\"]").getText();
            $x("//*[@name=\"captcha\"]").sendKeys(captcha);
            // $x("//*[@id=\"btnAirtimeATM\"]").click();
            resultsTest.put("6", new Object[]{
                    5d,
                    "Test trường hợp nạp tiền điện thoại thành công",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Sim, Thẻ cào.\n3- Scroll xuống chỗ Nạp tiền điện thoại\n5- Điền số điện thoại và chọn các option\n6- Nhập mã captcha\n7- Click vào button chọn Dùng thẻ ATM",
                    "Số điện thoại: 0795421952\nChọn nhà mạng: Mobifone\nChọn số tiền nạp: 50.000đ\n",
                    "Thông báo Nhập mã khách hàng",
                    "Thông báo Nhập mã khách hàng",
                    dtf.format(now),
                    "Pass",
            });
        } catch (AssertionError e) {
            resultsTest.put("6", new Object[]{
                    5d,
                    "Test trường hợp nạp tiền điện thoại thành công",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Sim, Thẻ cào.\n3- Scroll xuống chỗ Nạp tiền điện thoại\n5- Điền số điện thoại và chọn các option\n6- Nhập mã captcha\n7- Click vào button chọn Dùng thẻ ATM",
                    "Số điện thoại: 0795421952\nChọn nhà mạng: Mobifone\nChọn số tiền nạp: 50.000đ\n",
                    "Thông báo Nhập mã khách hàng",
                    "Không hiển thị thông báo Nhập mã khách hàng",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }

    @Test(priority = 7)
    public void mobileRechargeNotListActiveNetwork() {
        error = "";
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@href=\"/sim-so-dep\"]").click();
            $x("//*[@class=\"titlesimcard\"]").hover();
            $x("//*[@id=\"txtPhoneAirtime\"]").sendKeys("0795421952");
            // $x("//img[@src='/sim-so-dep/Content/desktop/images/brands/mobi.png']").click();
            $x("//*[@data-value=\"4103122001400\" and @data-price=\"50000\"]").click();
            // String captcha = "";
            // captcha =  $x("//*[@class=\"imgcaptcha\"]").getText();
            // $x("//*[@name=\"captcha\"]").sendKeys(captcha);
            // $x("//*[@id=\"btnAirtimeATM\"]").click();
            error = $x("//*[@id=\"error\"]").getText();
            if (error != "") {
                resultsTest.put("7", new Object[]{
                        6d,
                        "Test trường hợp nạp tiền điện thoại không chọn nhà mạng",
                        "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Sim, Thẻ cào.\n3- Scroll xuống chỗ Nạp tiền điện thoại\n5- Điền số điện thoại và chọn các option (Không chọn nhà mạng)",
                        "Số điện thoại: 0795421952\nChọn nhà mạng: Mobifone\nChọn số tiền nạp: 50.000đ\n",
                        "Thông báo "+error,
                        "Thông báo "+error,
                        dtf.format(now),
                        "Pass",
                });
            }
        } catch (AssertionError e) {
            resultsTest.put("7", new Object[]{
                    6d,
                    "Test trường hợp nạp tiền điện thoại không chọn nhà mạng",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Sim, Thẻ cào.\n3- Scroll xuống chỗ Nạp tiền điện thoại\n5- Điền số điện thoại và chọn các option (Không chọn nhà mạng)",
                    "Số điện thoại: 0795421952\nChọn nhà mạng: Mobifone\nChọn số tiền nạp: 50.000đ\n",
                    "Thông báo "+error,
                    "Không hiển thị thông báo "+error,
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }

    @Test(priority = 8)
    public void mobileRechargeNotPhone() {
        error = "";
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@href=\"/sim-so-dep\"]").click();
            $x("//*[@class=\"titlesimcard\"]").hover();
//            $x("//*[@id=\"txtPhoneAirtime\"]").sendKeys("0795421952");
            $x("//img[@src='/sim-so-dep/Content/desktop/images/brands/mobi.png']").click();
//            $x("//*[@data-value=\"4103122001400\" and @data-price=\"50000\"]").click();
            // String captcha = "";
            // captcha =  $x("//*[@class=\"imgcaptcha\"]").getText();
            // $x("//*[@name=\"captcha\"]").sendKeys(captcha);
            // $x("//*[@id=\"btnAirtimeATM\"]").click();
            error = $x("//*[@id=\"error\"]").getText();
            if (error != "") {
                resultsTest.put("8", new Object[]{
                        7d,
                        "Test trường hợp nạp tiền điện thoại không điền số điện thoại",
                        "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Sim, Thẻ cào.\n3- Scroll xuống chỗ Nạp tiền điện thoại\n5- Không Điền số điện thoại và chọn các option",
                        "Số điện thoại: 0795421952\nChọn nhà mạng: Mobifone\nChọn số tiền nạp: 50.000đ\n",
                        "Thông báo "+error,
                        "Thông báo "+error,
                        dtf.format(now),
                        "Pass",
                });
            }
        } catch (AssertionError e) {
            resultsTest.put("8", new Object[]{
                    7d,
                    "Test trường hợp nạp tiền điện thoại không điền số điện thoại",
                    "1- Vào trang https://www.thegioididong.com/\n2- Chọn Button Sim, Thẻ cào.\n3- Scroll xuống chỗ Nạp tiền điện thoại\n5- Không Điền số điện thoại và chọn các option",
                    "Số điện thoại: 0795421952\nChọn nhà mạng: Mobifone\nChọn số tiền nạp: 50.000đ\n",
                    "Thông báo "+error,
                    "Không hiển thị thông báo "+error,
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }

    @Test(priority = 9)
    public void WriteRateIpad() {
        try {
            open("https://www.thegioididong.com/");
            $x("//input[@class=\"input-search\"]").sendKeys("tablet");//key
            $x("//button[@type=\"submit\"]").click();//search
            $x("//div[@class=\"view-more \"]//a").hover();
            $x("//div[@class=\"view-more \"]//a").click();
            $x("//img[@alt=\"iPad Air 4 Wifi 256GB\"]").hover();
            $x("//img[@alt=\"iPad Air 4 Wifi 256GB\"]").click();
            $x("//a[@class=\"comment-btn__item\"]").click();
            $x("//textarea[@name=\"fRContent\"]").sendKeys("Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.");
            $x("//li[@data-val=\"5\"]").click();
            $x("//div[@class=\"txt-agree\"]").click();
            $x("//input[@name=\"fRName\"]").sendKeys("Nguyen Van An");//họ và tên
            $x("//input[@name=\"fRPhone\"]").sendKeys("0523757195");//phone
            $x("//a[@class=\"submit-assess\"]").click();
            resultsTest.put("9", new Object[]{8d, "Test Viết Đánh Giá Ipad Đúng tất cả", "1- Vào trang https://www.thegioididong.com/\n 2- Tìm Kiếm Tablet.\n3- Chọn Sản Phẩm\n 4- Click vào Button Viết Đánh Giá.\n 5-Điền Thông Tin", "Từ khóa tìm kiếm: Tablet\nGóp ý đánh giá sản phẩm:\"Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.\nRate:Rất Tốt\n Họ và tên: Nguyen Van An\n Số điện thoại:0523757195", "Thông báo kết quả bài viết", "Bài viết đang được xem xét", dtf.format(now), "Pass",});
        } catch (AssertionError e) {
            resultsTest.put("9", new Object[]{8d, "Test Viết Đánh Giá Ipad Đúng tất cả", "1- Vào trang https://www.thegioididong.com/\n 2- Tìm Kiếm Tablet.\n3- Chọn Sản Phẩm\n 4- Click vào Button Viết Đánh Giá.\n 5-Điền Thông Tin", "Từ khóa tìm kiếm: Tablet\nGóp ý đánh giá sản phẩm:\"Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.\nRate:Rất Tốt\n Họ và tên: Nguyen Van An\n Số điện thoại:0523757195", "Thông báo kết quả bài viết", "Bài viết đang được xem xét", dtf.format(now), "Fail",});
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 10)
    public void WriteRateIpadNoComment() {//error
        error="";
        try {
            open("https://www.thegioididong.com/");
            $x("//input[@class=\"input-search\"]").sendKeys("tablet");//key
            $x("//button[@type=\"submit\"]").click();//search
            $x("//div[@class=\"view-more \"]//a").hover();
            $x("//div[@class=\"view-more \"]//a").click();
            $x("//img[@alt=\"Lenovo Tab M8 (TB-8505X)\"]").hover();
            $x("//img[@alt=\"Lenovo Tab M8 (TB-8505X)\"]").click();
            $x("//a[@class=\"comment-btn__item\"]").click();
            //$x("//textarea[@name=\"fRContent\"]").sendKeys("Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.");
            $x("//li[@data-val=\"5\"]").click();
            $x("//div[@class=\"txt-agree\"]").click();
            $x("//input[@name=\"fRName\"]").sendKeys("Nguyen Van An");//họ và tên
            $x("//input[@name=\"fRPhone\"]").sendKeys("0523757195");//phone
            $x("//a[@class=\"submit-assess\"]").click();
            //$x("//div[@class=\"read-assess-close btn-closemenu\"]").click();
            error=$x("//span[@class=\"lbMsgRt\"]").getText();
            if (error != ""){
                resultsTest.put("10", new Object[]{9d, "Test Viết Đánh Giá Ipad Không Comment", "1- Vào trang https://www.thegioididong.com/\n 2- Tìm Kiếm Tablet.\n3- Chọn Sản Phẩm\n 4- Click vào Button Viết Đánh Giá.\n 5-Điền Thông Tin", "Từ khóa tìm kiếm: Tablet\n Góp ý đánh giá sản phẩm: Để trống\nRate:Rất Tốt\n Họ và tên: Nguyen Van An\n Số điện thoại:0523757195", "Kết quả bài viết" +error, "Thông báo:" +error, dtf.format(now), "Pass",});
            }
        } catch (AssertionError e) {
            resultsTest.put("10", new Object[]{9d, "Test Viết Đánh Giá Ipad Không Comment", "1- Vào trang https://www.thegioididong.com/\n 2- Tìm Kiếm Tablet.\n3- Chọn Sản Phẩm\n 4- Click vào Button Viết Đánh Giá.\n 5-Điền Thông Tin", "Từ khóa tìm kiếm: Tablet\n Góp ý đánh giá sản phẩm: Để trống\nRate:Rất Tốt\n Họ và tên: Nguyen Van An\n Số điện thoại:0523757195", "Kết quả bài viết" +error, "Thông báo:" +error, dtf.format(now), "Fail",});
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 11)
    public void WriteRateIpadNoCommentNotValue() {//error//thiếu kí tự
        error="";
        try {
            open("https://www.thegioididong.com/");
            $x("//input[@class=\"input-search\"]").sendKeys("tablet");//key
            $x("//button[@type=\"submit\"]").click();//search
            $x("//div[@class=\"view-more \"]//a").hover();
            $x("//div[@class=\"view-more \"]//a").click();
            $x("//img[@alt=\"Samsung Galaxy Tab S6 Lite\"]").hover();
            $x("//img[@alt=\"Samsung Galaxy Tab S6 Lite\"]").click();
            $x("//a[@class=\"comment-btn__item\"]").click();
            $x("//textarea[@name=\"fRContent\"]").sendKeys("Sản phẩm tốt.");
            $x("//li[@data-val=\"5\"]").click();
            $x("//div[@class=\"txt-agree\"]").click();
            $x("//input[@name=\"fRName\"]").sendKeys("Nguyen Van An");//họ và tên
            $x("//input[@name=\"fRPhone\"]").sendKeys("0523757195");//phone
            $x("//a[@class=\"submit-assess\"]").click();
            //$x("//div[@class=\"read-assess-close btn-closemenu\"]").click();
            error=$x("//span[@class=\"lbMsgRt\"]").getText();
            if (error != ""){
                resultsTest.put("11", new Object[]{10d, "Test Viết Đánh Giá Ipad Thiếu Kí Tự Comment", "1- Vào trang https://www.thegioididong.com/\n 2- Tìm Kiếm Tablet.\n3- Chọn Sản Phẩm\n 4- Click vào Button Viết Đánh Giá.\n 5-Điền Thông Tin", "Từ khóa tìm kiếm: Tablet\nGóp ý đánh giá sản phẩm:Sản phẩm tốt\nRate:Rất Tốt\n Họ và tên: Nguyen Van An\n Số điện thoại:0523757195", "Kết quả bài viết"+error, "Thông báo:"+error, dtf.format(now), "Pass",});
            }
        } catch (AssertionError e) {
            resultsTest.put("11", new Object[]{10d, "Test Viết Đánh Giá Ipad Thiếu Kí Tự Comment", "1- Vào trang https://www.thegioididong.com/\n 2- Tìm Kiếm Tablet.\n3- Chọn Sản Phẩm\n 4- Click vào Button Viết Đánh Giá.\n 5-Điền Thông Tin", "Từ khóa tìm kiếm: Tablet\nGóp ý đánh giá sản phẩm:Sản phẩm tốt\nRate:Rất Tốt\n Họ và tên: Nguyen Van An\n Số điện thoại:0523757195", "Kết quả bài viết"+error, "Thông báo:"+error, dtf.format(now), "Fail",});
            Assert.assertTrue(false);
        }
    }

    @Test(priority = 12)
    public void WriteRateIpadNoCommentNotValuePhone() {//error//số điện thoại thiếu//fail
        try {
            open("https://www.thegioididong.com/");
            $x("//input[@class=\"input-search\"]").sendKeys("tablet");//key
            $x("//button[@type=\"submit\"]").click();//search
            $x("//div[@class=\"view-more \"]//a").hover();
            $x("//div[@class=\"view-more \"]//a").click();
            $x("//img[@alt=\"iPad 8 Wifi 128GB (2020)\"]").hover();
            $x("//img[@alt=\"iPad 8 Wifi 128GB (2020)\"]").click();
            $x("//a[@class=\"comment-btn__item\"]").click();
            $x("//textarea[@name=\"fRContent\"]").sendKeys("Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.");
            $x("//li[@data-val=\"5\"]").click();
            $x("//div[@class=\"txt-agree\"]").click();
            $x("//input[@name=\"fRName\"]").sendKeys("Nguyen Van An");//họ và tên
            $x("//input[@name=\"fRPhone\"]").sendKeys("0523");//phone
            $x("//a[@class=\"submit-assess\"]").click();
            //$x("//div[@class=\"read-assess-close btn-closemenu\"]").click();
            resultsTest.put("12", new Object[]{11d, "Test Viết Đánh Giá Ipad NumberPhone: Không Đúng Định Dạng", "1- Vào trang https://www.thegioididong.com/\n 2- Tìm Kiếm Tablet.\n3- Chọn Sản Phẩm\n 4- Click vào Button Viết Đánh Giá.\n 5-Điền Thông Tin", "Từ khóa tìm kiếm: Tablet\nGóp ý đánh giá sản phẩm:Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.\nRate:Rất Tốt\n Họ và tên: Nguyen Van An\n Số điện thoại:0523", "Thông báo kết quả", "Số điện thoại không hợp lệ", dtf.format(now), "Pass",});
        } catch (AssertionError e) {
            resultsTest.put("12", new Object[]{11d, "Test Viết Đánh Giá Ipad NumberPhone: Không Đúng Định Dạng", "1- Vào trang https://www.thegioididong.com/\n 2- Tìm Kiếm Tablet.\n3- Chọn Sản Phẩm\n 4- Click vào Button Viết Đánh Giá.\n 5-Điền Thông Tin", "Từ khóa tìm kiếm: Tablet\nGóp ý đánh giá sản phẩm:Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.\nRate:Rất Tốt\n Họ và tên: Nguyen Van An\n Số điện thoại:0523", "Thông báo kết quả", "Số điện thoại không hợp lệ", dtf.format(now), "Fail",});
            Assert.assertTrue(true);
        }
    }
    @Test(priority = 13)
    public void WriteRateIpadNoCommentNotValueName() {//error//name k đúng định dạng//fail
        try {
            open("https://www.thegioididong.com/");
            $x("//input[@class=\"input-search\"]").sendKeys("tablet");//key
            $x("//button[@type=\"submit\"]").click();//search
            $x("//div[@class=\"view-more \"]//a").hover();
            $x("//div[@class=\"view-more \"]//a").click();
            $x("//img[@alt=\"Samsung Galaxy Tab A7 (2020)\"]").hover();
            $x("//img[@alt=\"Samsung Galaxy Tab A7 (2020)\"]").click();
            $x("//a[@class=\"comment-btn__item\"]").click();
            $x("//textarea[@name=\"fRContent\"]").sendKeys("Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.");
            $x("//li[@data-val=\"5\"]").click();
            $x("//div[@class=\"txt-agree\"]").click();
            $x("//input[@name=\"fRName\"]").sendKeys("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");//họ và tên
            $x("//input[@name=\"fRPhone\"]").sendKeys("0523757195");//phone
            $x("//a[@class=\"submit-assess\"]").click();
            //$x("//div[@class=\"read-assess-close btn-closemenu\"]").click();
            resultsTest.put("13", new Object[]{12d, "Test Viết Đánh Giá Ipad Name : Kí tự đặt biệt", "1- Vào trang https://www.thegioididong.com/\n 2- Tìm Kiếm Tablet.\n3- Chọn Sản Phẩm\n 4- Click vào Button Viết Đánh Giá.\n 5-Điền Thông Tin", "Từ khóa tìm kiếm: Tablet\nGóp ý đánh giá sản phẩm:Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.\nRate:Rất Tốt\n Họ và tên:@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n Số điện thoại:0523757195", "Kết quả bài viết", "Thông báo bài viết đang được kiểm duyệt", dtf.format(now), "Pass",});
        } catch (AssertionError e) {
            resultsTest.put("13", new Object[]{12d, "Test Viết Đánh Giá Ipad Name : Kí tự đặt biệt", "1- Vào trang https://www.thegioididong.com/\n 2- Tìm Kiếm Tablet.\n3- Chọn Sản Phẩm\n 4- Click vào Button Viết Đánh Giá.\n 5-Điền Thông Tin", "Từ khóa tìm kiếm: Tablet\nGóp ý đánh giá sản phẩm:Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.\nRate:Rất Tốt\n Họ và tên:@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n Số điện thoại:0523757195", "Kết quả bài viết", "Thông báo bài viết đang được kiểm duyệt", dtf.format(now), "Fail",});
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 14)
    public void WriteRateIpadNoCommentALl() {//error
        error="";
        try {
            open("https://www.thegioididong.com/");
            $x("//input[@class=\"input-search\"]").sendKeys("tablet");//key
            $x("//button[@type=\"submit\"]").click();//search
            //$x("//div[@class=\"view-more \"]//a").hover();
            //$x("//div[@class=\"view-more \"]//a").click();
            $x("//img[@alt=\"iPad Pro 11 inch Wifi Cellular 128GB (2020)\"]").hover();
            $x("//img[@alt=\"iPad Pro 11 inch Wifi Cellular 128GB (2020)\"]").click();
            $x("//a[@class=\"comment-btn__item\"]").click();
            //$x("//textarea[@name=\"fRContent\"]").sendKeys("Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.");
            // $x("//li[@data-val=\"5\"]").click();
            //$x("//div[@class=\"txt-agree\"]").click();
            //$x("//input[@name=\"fRName\"]").sendKeys("Nguyen Van An");//họ và tên
            //$x("//input[@name=\"fRPhone\"]").sendKeys("0523757195");//phone
            $x("//a[@class=\"submit-assess\"]").click();
            //$x("//div[@class=\"read-assess-close btn-closemenu\"]").click();
            error=$x("//span[@class=\"lbMsgRt\"]").getText();
            if (error != ""){
                resultsTest.put("14", new Object[]{13d, "Test Viết Đánh Giá Ipad Bỏ Trống Tất Cả Thuộc Tính", "1- Vào trang https://www.thegioididong.com/\n 2- Tìm Kiếm Tablet.\n3- Chọn Sản Phẩm\n 4- Click vào Button Viết Đánh Giá.\n 5-Điền Thông Tin", "Từ khóa tìm kiếm: Tablet\n Góp ý đánh giá sản phẩm:Để trống\n Họ và Tên: Để trống\n Số điện thoại: Để trống", "Kết quả bài viết"+error, "Thông báo:"+error, dtf.format(now), "Pass",});
            }
        } catch (AssertionError e) {
            resultsTest.put("14", new Object[]{13d, "Test Viết Đánh Giá Ipad Bỏ Trống Tất Cả Thuộc Tính", "1- Vào trang https://www.thegioididong.com/\n 2- Tìm Kiếm Tablet.\n3- Chọn Sản Phẩm\n 4- Click vào Button Viết Đánh Giá.\n 5-Điền Thông Tin", "Từ khóa tìm kiếm: Tablet\n Góp ý đánh giá sản phẩm:Để trống\n Họ và Tên: Để trống\n Số điện thoại: Để trống", "Kết quả bài viết"+error, "Thông báo:"+error, dtf.format(now), "Fail",});
            Assert.assertTrue(false);
        }
    }

    @Test(priority = 15)
    public void WriteRateIpadNoCommentName() {//error
        error="";
        try {
            open("https://www.thegioididong.com/");
            $x("//input[@class=\"input-search\"]").sendKeys("tablet");//key
            $x("//button[@type=\"submit\"]").click();//search
            $x("//div[@class=\"view-more \"]//a").hover();
            $x("//div[@class=\"view-more \"]//a").click();
            $x("//img[@alt=\"Huawei MatePad 64GB (Nền tảng Huawei Mobile Service)\"]").hover();
            $x("//img[@alt=\"Huawei MatePad 64GB (Nền tảng Huawei Mobile Service)\"]").click();
            $x("//a[@class=\"comment-btn__item\"]").click();
            $x("//textarea[@name=\"fRContent\"]").sendKeys("Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.");
            $x("//li[@data-val=\"5\"]").click();
            $x("//div[@class=\"txt-agree\"]").click();
            //$x("//input[@name=\"fRName\"]").sendKeys("Nguyen Van An");//họ và tên
            $x("//input[@name=\"fRPhone\"]").sendKeys("0523757195");//phone
            $x("//a[@class=\"submit-assess\"]").click();
            // $x("//div[@class=\"read-assess-close btn-closemenu\"]").click();
            error=$x("//span[@class=\"lbMsgRt\"]").getText();
            if (error != ""){
                resultsTest.put("15", new Object[]{14d, "Test Viết Đánh Giá Ipad Tên: Để trống", "1- Vào trang https://www.thegioididong.com/\n 2- Tìm Kiếm Tablet.\n3- Chọn Sản Phẩm\n 4- Click vào Button Viết Đánh Giá.\n 5-Điền Thông Tin", "Từ khóa tìm kiếm: Tablet\nGóp ý đánh giá sản phẩm:Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.\nRate:Rất Tốt\n  Họ và Tên:Để trống\n Số điện thoại:0523757195", "Kết quả bài viết"+error, "Thông báo"+error, dtf.format(now), "Pass",});
            }
        } catch (AssertionError e) {
            resultsTest.put("15", new Object[]{14d, "Test Viết Đánh Giá Ipad Tên: Để trống", "1- Vào trang https://www.thegioididong.com/\n 2- Tìm Kiếm Tablet.\n3- Chọn Sản Phẩm\n 4- Click vào Button Viết Đánh Giá.\n 5-Điền Thông Tin", "Từ khóa tìm kiếm: Tablet\nGóp ý đánh giá sản phẩm:Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.\nRate:Rất Tốt\n Họ và Tên:Để trống\n Số điện thoại:0523757195", "Kết quả bài viết"+error, "Thông báo"+error, dtf.format(now), "Fail",});
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 16)
    public void WriteRateIpadNoCommentNumberPhone() {//error
        error="";
        try {
            open("https://www.thegioididong.com/");
            $x("//input[@class=\"input-search\"]").sendKeys("tablet");//key
            $x("//button[@type=\"submit\"]").click();//search
            $x("//div[@class=\"view-more \"]//a").hover();
            $x("//div[@class=\"view-more \"]//a").click();
            $x("//img[@alt=\"Samsung Galaxy Tab S7\"]").hover();
            $x("//img[@alt=\"Samsung Galaxy Tab S7\"]").click();
            $x("//a[@class=\"comment-btn__item\"]").click();
            $x("//textarea[@name=\"fRContent\"]").sendKeys("Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.");
            $x("//li[@data-val=\"5\"]").click();
            $x("//div[@class=\"txt-agree\"]").click();
            $x("//input[@name=\"fRName\"]").sendKeys("Nguyen Van An");//họ và tên
            //$x("//input[@name=\"fRPhone\"]").sendKeys("0523757195");//phone
            $x("//a[@class=\"submit-assess\"]").click();
            //$x("//div[@class=\"read-assess-close btn-closemenu\"]").click();
            error=$x("//span[@class=\"lbMsgRt\"]").getText();
            if (error != ""){
                resultsTest.put("16", new Object[]{15d, "Test Viết Đánh Giá Ipad Numberphone:Để trống", "1- Vào trang https://www.thegioididong.com/\n 2- Tìm Kiếm Tablet.\n3- Chọn Sản Phẩm\n 4- Click vào Button Viết Đánh Giá.\n 5-Điền Thông Tin", "Từ khóa tìm kiếm: Tablet\nGóp ý đánh giá sản phẩm:Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.\nRate:Rất Tốt\n Họ và Tên:Nguyen Van An\n Số điện thoại:Để trống", "Kết quả bài viết"+error, "Thông báo"+error, dtf.format(now), "Pass",});
            }
        } catch (AssertionError e) {
            resultsTest.put("16", new Object[]{15d, "Test Viết Đánh Giá Ipad Numberphone: Để trống", "1- Vào trang https://www.thegioididong.com/\n 2- Tìm Kiếm Tablet.\n3- Chọn Sản Phẩm\n 4- Click vào Button Viết Đánh Giá.\n 5-Điền Thông Tin", "Từ khóa tìm kiếm: Tablet\nGóp ý đánh giá sản phẩm:Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.\nRate:Rất Tốt\n Họ và Tên:Nguyen Van An\n Số điện thoại:Để trống", "Kết quả bài viết"+error, "Thông báo"+error, dtf.format(now), "Fail",});
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 17)
    public void WriteRateIpadNoRate() {//error
        error="";
        try {
            open("https://www.thegioididong.com/");
            $x("//input[@class=\"input-search\"]").sendKeys("tablet");//key
            $x("//button[@type=\"submit\"]").click();//search
            $x("//div[@class=\"view-more \"]//a").hover();
            $x("//div[@class=\"view-more \"]//a").click();
            $x("//img[@data-src=\"https://cdn.tgdd.vn/Products/Images/522/205751/samsung-galaxy-tab-a8-t295-2019-silver-1-600x600.jpg\"]").hover();
            $x("//img[@data-src=\"https://cdn.tgdd.vn/Products/Images/522/205751/samsung-galaxy-tab-a8-t295-2019-silver-1-600x600.jpg\"]").click();
            $x("//a[@class=\"comment-btn__item\"]").click();
            $x("//textarea[@name=\"fRContent\"]").sendKeys("Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.");
            //$x("//li[@data-val=\"5\"]").click();
            $x("//div[@class=\"txt-agree\"]").click();
            $x("//input[@name=\"fRName\"]").sendKeys("Nguyen Van An");//họ và tên
            $x("//input[@name=\"fRPhone\"]").sendKeys("0523757195");//phone
            $x("//a[@class=\"submit-assess\"]").click();
            // $x("//div[@class=\"read-assess-close btn-closemenu\"]").click();
            error=$x("//span[@class=\"lbMsgRt\"]").getText();
            if (error != ""){
                resultsTest.put("17", new Object[]{16d, "Test Viết Đánh Giá Ipad Rate:Để trống", "1- Vào trang https://www.thegioididong.com/\n 2- Tìm Kiếm Tablet.\n3- Chọn Sản Phẩm\n 4- Click vào Button Viết Đánh Giá.\n 5-Điền Thông Tin", "Từ khóa tìm kiếm: Tablet\nGóp ý đánh giá sản phẩm:Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.\nRate:Để trống\n Họ và Tên:Nguyen Van An\n Số điện thoại:Để trống", "Kết quả bài viết"+error, "Thông báo"+error, dtf.format(now), "Pass",});
            }
        } catch (AssertionError e) {
            resultsTest.put("17", new Object[]{16d, "Test Viết Đánh Giá Ipad Rate: Để trống", "1- Vào trang https://www.thegioididong.com/\n 2- Tìm Kiếm Tablet.\n3- Chọn Sản Phẩm\n 4- Click vào Button Viết Đánh Giá.\n 5-Điền Thông Tin", "Từ khóa tìm kiếm: Tablet\nGóp ý đánh giá sản phẩm:Sản phẩm tốt trong tầm giá với nhiều mẫu có nhiều sự lựa chọn khác nhau phù hợp với nhu cầu sử dụng.\nRate:Để trống\n Họ và Tên:Nguyen Van An\n Số điện thoại:Để trống", "Kết quả bài viết"+error, "Thông báo"+error, dtf.format(now), "Fail",});
            Assert.assertTrue(false);
        }
    }

    @Test(priority = 18)
    public void Ratesmartwatch() {
        error="";
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@id=\"skw\"]").sendKeys("Đồng hồ thông minh");
            $x("//button[@type=\"submit\"]").click();
            $x("//div[@class=\"view-more \"]//a").hover();
            $x("//div[@class=\"view-more \"]//a").click();
            $x("//a[@data-name=\"Apple Watch S5 44mm viền nhôm dây cao su đen\"]").hover();
            $x("//a[@data-name=\"Apple Watch S5 44mm viền nhôm dây cao su đen\"]").click();
            $x("//a[@href=\"javascript:void(0)\" and @class=\"btn-detail jsArticle\"]").hover();
            $x("//a[@href=\"javascript:void(0)\" and @class=\"btn-detail jsArticle\"]").click();
            $x("//button[@name=\"rating-good\"]").hover();
            $x("//button[@name=\"rating-good\"]").click();//hữu ích
            $x("//textarea[@class=\"article-rating__reason__input\"]").sendKeys("Sản phẩm tốt");
            $x("//div[@class=\"article-rating__reason__dropdown dropdown-age\"]").click();//độ tuổi
            $x("//div[@class=\"article-rating__reason__dropdown__data\"]//p[text()=\"18-24\"]").click();//18-24
            $x("//div[@class=\"article-rating__reason__dropdown dropdown-gender\"]").click();//giới tính
            $x("//div[@class=\"article-rating__reason__dropdown__data\"]//p[@data-value=\"1\"]").click();//nam
            $x("//button[@name=\"main-button\"]").click();//submit
            //$x("//div[@class=\"btn-closemenu close-tab\"]").click();//dóng
            error=$x("//div[@class=\"article-rating__thank__left\"]//b[text()='Cảm ơn về thông tin bạn đã chia sẻ.']").getText();
            if (error != ""){
                resultsTest.put("18", new Object[]{17d, "Test Đánh Giá bài viết SmartWatch", "1- Vào trang https://www.thegioididong.com/\n2- Tìm kiếm Đồng Hồ Thông Minh.\n3- Chọn Sản Phẩm\n4- Click vào Button Xem thêm.\n 5- Đánh giá hữu ích.\n 6- Góp ý đánh giá sản phẩm", "Từ khóa tìm kiếm: Đồng Hồ Thông Minh\nĐánh giá Hài Lòng\nGóp ý:Sản Phẩm Tốt\n Độ tuổi:18-24\n Giới tính: Nam", "Hiển thị kết quả đánh giá","Thông báo:" +error, dtf.format(now), "Pass",});
            }
        } catch (AssertionError e) {
            resultsTest.put("18", new Object[]{17d, "Test Đánh Giá bài viết SmartWatch", "1- Vào trang https://www.thegioididong.com/\n2- Tìm kiếm Đồng Hồ Thông Minh.\n3- Chọn Sản Phẩm\n4- Click vào Button Xem thêm.\n 5- Đánh giá hữu ích.\n 6- Góp ý đánh giá sản phẩm", "Từ khóa tìm kiếm: Đồng Hồ Thông Minh\nĐánh giá Hài Lòng\nGóp ý:Sản Phẩm Tốt\n Độ tuổi:18-24\n Giới tính: Nam", "Hiển thị kết quả đánh giá","Thông báo:" +error, dtf.format(now), "Fail",});
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 19)
    public void Ratesmartwatchnocomemntnoagenogender() {
        error="";
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@id=\"skw\"]").sendKeys("Đồng hồ thông minh");
            $x("//button[@type=\"submit\"]").click();
            $x("//div[@class=\"view-more \"]//a").hover();
            $x("//div[@class=\"view-more \"]//a").click();
            $x("//img[@alt=\"BeU B2\"]").hover();
            $x("//img[@alt=\"BeU B2\"]").click();
            $x("//a[@href=\"javascript:void(0)\" and @class=\"btn-detail jsArticle\"]").hover();
            $x("//a[@href=\"javascript:void(0)\" and @class=\"btn-detail jsArticle\"]").click();
            $x("//button[@name=\"rating-bad\"]").hover();
            $x("//button[@name=\"rating-bad\"]").click();// K hữu ích
            //$x("//textarea[@class=\"article-rating__reason__input\"]").sendKeys("Sản phẩm tốt");
            //$x("//div[@class=\"article-rating__reason__dropdown dropdown-age\"]").click();//độ tuổi
            //$x("//div[@class=\"article-rating__reason__dropdown__data\"]//p[text()=\"18-24\"]").click();//18-24
            //$x("//div[@class=\"article-rating__reason__dropdown dropdown-gender\"]").click();//giới tính
            //$x("//div[@class=\"article-rating__reason__dropdown__data\"]//p[@data-value=\"1\"]").click();//nam
            $x("//button[@name=\"main-button\"]").click();//submit
            // $x("//div[@class=\"btn-closemenu close-tab\"]").click();//dóng
            error=$x("//label[@id=\"inputContent-error\"]").getText();
            if (error != ""){
                resultsTest.put("19", new Object[]{18d, "Test Đánh Giá bài viết SmartWatch Comment,Tuổi,Giới Tính: Để trống", "1- Vào trang https://www.thegioididong.com/\n2- Tìm kiếm Đồng Hồ Thông Minh.\n3- Chọn Sản Phẩm\n4- Click vào Button Xem thêm.\n 5- Đánh giá Không hữu ích", "Từ khóa tìm kiếm: Đồng Hồ Thông Minh\nĐánh giá Không Hài Lòng", "Thông báo:" +error,"Thông báo:" +error, dtf.format(now), "Pass",});
            }
        } catch (AssertionError e) {
            resultsTest.put("19", new Object[]{18d, "Test Đánh Giá bài viết SmartWatch Comment,Tuổi,Giới Tính: Để trống", "1- Vào trang https://www.thegioididong.com/\n2- Tìm kiếm Đồng Hồ Thông Minh.\n3- Chọn Sản Phẩm\n4- Click vào Button Xem thêm.\n 5- Đánh giá Không hữu ích", "Từ khóa tìm kiếm: Đồng Hồ Thông Minh\nĐánh giá Không Hài Lòng", "Thông báo:" +error,"Thông báo:" +error, dtf.format(now), "Fail",});        }
        //Assert.assertTrue(true);
    }

    @Test(priority = 20)
    public void RatesmartwatchNoAge() {
        error="";
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@id=\"skw\"]").sendKeys("Đồng hồ thông minh");
            $x("//button[@type=\"submit\"]").click();
            $x("//div[@class=\"view-more \"]//a").hover();
            $x("//div[@class=\"view-more \"]//a").click();
            $x("//img[@alt=\"Samsung Galaxy Watch Active 2 44mm viền thép dây da\"]").hover();
            $x("//img[@alt=\"Samsung Galaxy Watch Active 2 44mm viền thép dây da\"]").click();
            $x("//a[@href=\"javascript:void(0)\" and @class=\"btn-detail jsArticle\"]").hover();
            $x("//a[@href=\"javascript:void(0)\" and @class=\"btn-detail jsArticle\"]").click();
            $x("//button[@name=\"rating-good\"]").hover();
            $x("//button[@name=\"rating-good\"]").click();//hữu ích
            $x("//textarea[@class=\"article-rating__reason__input\"]").sendKeys("Sản phẩm tốt");
            //$x("//div[@class=\"article-rating__reason__dropdown dropdown-age\"]").click();//độ tuổi
            //$x("//div[@class=\"article-rating__reason__dropdown__data\"]//p[text()=\"18-24\"]").click();//18-24
            $x("//div[@class=\"article-rating__reason__dropdown dropdown-gender\"]").click();//giới tính
            $x("//div[@class=\"article-rating__reason__dropdown__data\"]//p[@data-value=\"1\"]").click();//nam
            $x("//button[@name=\"main-button\"]").click();//submit
            //$x("//div[@class=\"btn-closemenu close-tab\"]").click();//dóng
            error=$x("//div[@class=\"article-rating__thank__left\"]//b[text()='Cảm ơn về thông tin bạn đã chia sẻ.']").getText();
            if (error != ""){
                resultsTest.put("20", new Object[]{19d, "Test Đánh Giá bài viết SmartWatch Tuổi: Để trống", "1- Vào trang https://www.thegioididong.com/\n2- Tìm kiếm Đồng Hồ Thông Minh.\n3- Chọn Sản Phẩm\n4- Click vào Button Xem thêm.\n 5- Đánh giá hữu ích.\n 6- Góp ý đánh giá sản phẩm", "Từ khóa tìm kiếm: Đồng Hồ Thông Minh\nĐánh giá Hài Lòng\nGóp ý:Sản Phẩm Tốt\nGiới tính: Nam", "Hiển thị kết quả đánh giá","Thông báo:" +error, dtf.format(now), "Pass",});

            }
        } catch (AssertionError e) {
            resultsTest.put("20", new Object[]{19d, "Test Đánh Giá bài viết SmartWatch Tuổi: Để trống", "1- Vào trang https://www.thegioididong.com/\n2- Tìm kiếm Đồng Hồ Thông Minh.\n3- Chọn Sản Phẩm\n4- Click vào Button Xem thêm.\n 5- Đánh giá hữu ích.\n 6- Góp ý đánh giá sản phẩm", "Từ khóa tìm kiếm: Đồng Hồ Thông Minh\nĐánh giá Hài Lòng\nGóp ý:Sản Phẩm Tốt\nGiới tính: Nam", "Hiển thị kết quả đánh giá","Thông báo:" +error, dtf.format(now), "Fail",});
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 21)
    public void RatesmartwatchNoGender() {
        error="";
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@id=\"skw\"]").sendKeys("Đồng hồ thông minh");
            $x("//button[@type=\"submit\"]").click();
            $x("//div[@class=\"view-more \"]//a").hover();
            $x("//div[@class=\"view-more \"]//a").click();
            $x("//img[@alt=\"Xiaomi Mi Watch\"]").hover();
            $x("//img[@alt=\"Xiaomi Mi Watch\"]").click();
            $x("//a[@href=\"javascript:void(0)\" and @class=\"btn-detail jsArticle\"]").hover();
            $x("//a[@href=\"javascript:void(0)\" and @class=\"btn-detail jsArticle\"]").click();
            $x("//button[@name=\"rating-good\"]").hover();
            $x("//button[@name=\"rating-good\"]").click();//hữu ích
            $x("//textarea[@class=\"article-rating__reason__input\"]").sendKeys("Sản phẩm tốt");
            $x("//div[@class=\"article-rating__reason__dropdown dropdown-age\"]").click();//độ tuổi
            $x("//div[@class=\"article-rating__reason__dropdown__data\"]//p[text()=\"18-24\"]").click();//18-24
            //$x("//div[@class=\"article-rating__reason__dropdown dropdown-gender\"]").click();//giới tính
            //$x("//div[@class=\"article-rating__reason__dropdown__data\"]//p[@data-value=\"1\"]").click();//nam
            $x("//button[@name=\"main-button\"]").click();//submit
            //$x("//div[@class=\"btn-closemenu close-tab\"]").click();//dóng
            error=$x("//div[@class=\"article-rating__thank__left\"]//b[text()='Cảm ơn về thông tin bạn đã chia sẻ.']").getText();
            if (error != ""){
                resultsTest.put("21", new Object[]{20d, "Test Đánh Giá bài viết SmartWatch Giới Tính:Để trống", "1- Vào trang https://www.thegioididong.com/\n2- Tìm kiếm Đồng Hồ Thông Minh.\n3- Chọn Sản Phẩm\n4- Click vào Button Xem thêm.\n 5- Đánh giá hữu ích.\n 6- Góp ý đánh giá sản phẩm", "Từ khóa tìm kiếm: Đồng Hồ Thông Minh\nĐánh giá Hài Lòng\nGóp ý:Sản Phẩm Tốt\nTuổi: 18-24", "Hiển thị kết quả đánh giá","Thông báo:"+error, dtf.format(now), "Pass",});
            }
        } catch (AssertionError e) {
            resultsTest.put("21", new Object[]{20d, "Test Đánh Giá bài viết SmartWatch Giới Tính:Để trống", "1- Vào trang https://www.thegioididong.com/\n2- Tìm kiếm Đồng Hồ Thông Minh.\n3- Chọn Sản Phẩm\n4- Click vào Button Xem thêm.\n 5- Đánh giá hữu ích.\n 6- Góp ý đánh giá sản phẩm", "Từ khóa tìm kiếm: Đồng Hồ Thông Minh\nĐánh giá Hài Lòng\nGóp ý:Sản Phẩm Tốt\nTuổi: 18-24", "Hiển thị kết quả đánh giá","Thông báo:"+error, dtf.format(now), "Fail",});
            Assert.assertTrue(false);
        }
    }

    @Test(priority = 22)
    public void RatesmartwatchNoGenderNoAge() {
        error="";
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@id=\"skw\"]").sendKeys("Đồng hồ thông minh");
            $x("//button[@type=\"submit\"]").click();
            $x("//div[@class=\"view-more \"]//a").hover();
            $x("//div[@class=\"view-more \"]//a").click();
            $x("//img[@alt=\"Đồng hồ thông minh định vị trẻ em Kidcare 08S\"]").hover();
            $x("//img[@alt=\"Đồng hồ thông minh định vị trẻ em Kidcare 08S\"]").click();
            $x("//a[@href=\"javascript:void(0)\" and @class=\"btn-detail jsArticle\"]").hover();
            $x("//a[@href=\"javascript:void(0)\" and @class=\"btn-detail jsArticle\"]").click();
            $x("//button[@name=\"rating-good\"]").hover();
            $x("//button[@name=\"rating-good\"]").click();//hữu ích
            $x("//textarea[@class=\"article-rating__reason__input\"]").sendKeys("Sản phẩm tốt");
            //$x("//div[@class=\"article-rating__reason__dropdown dropdown-age\"]").click();//độ tuổi
            //$x("//div[@class=\"article-rating__reason__dropdown__data\"]//p[text()=\"18-24\"]").click();//18-24
            //$x("//div[@class=\"article-rating__reason__dropdown dropdown-gender\"]").click();//giới tính
            //$x("//div[@class=\"article-rating__reason__dropdown__data\"]//p[@data-value=\"1\"]").click();//nam
            $x("//button[@name=\"main-button\"]").click();//submit
            //$x("//div[@class=\"btn-closemenu close-tab\"]").click();//dóng
            error=$x("//div[@class=\"article-rating__thank__left\"]//b[text()='Cảm ơn về thông tin bạn đã chia sẻ.']").getText();
            if (error != ""){
                resultsTest.put("22", new Object[]{21d, "Test Đánh Giá bài viết SmartWatch Tuổi,Giới Tính: Để trống", "1- Vào trang https://www.thegioididong.com/\n2- Tìm kiếm Đồng Hồ Thông Minh.\n3- Chọn Sản Phẩm\n4- Click vào Button Xem thêm.\n 5- Đánh giá hữu ích.\n 6- Góp ý đánh giá sản phẩm", "Từ khóa tìm kiếm: Đồng Hồ Thông Minh\nĐánh giá Hài Lòng\nGóp ý:Sản Phẩm Tốt", "Hiển thị kết quả đánh giá","Thông báo:" +error, dtf.format(now), "Pass",});
            }
        } catch (AssertionError e) {
            resultsTest.put("22", new Object[]{21d, "Test Đánh Giá bài viết SmartWatch Tuổi,Giới Tính: Để trống", "1- Vào trang https://www.thegioididong.com/\n2- Tìm kiếm Đồng Hồ Thông Minh.\n3- Chọn Sản Phẩm\n4- Click vào Button Xem thêm.\n 5- Đánh giá hữu ích.\n 6- Góp ý đánh giá sản phẩm", "Từ khóa tìm kiếm: Đồng Hồ Thông Minh\nĐánh giá Hài Lòng\nGóp ý:Sản Phẩm Tốt", "Hiển thị kết quả đánh giá","Thông báo:" +error, dtf.format(now), "Fail",});
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 23)
    public void Ratesmartwatchnocomemnt() {//sai
        error="";
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@id=\"skw\"]").sendKeys("Đồng hồ thông minh");
            $x("//button[@type=\"submit\"]").click();
            $x("//div[@class=\"view-more \"]//a").hover();
            $x("//div[@class=\"view-more \"]//a").click();
            $x("//img[@alt=\"Huawei Watch Fit dây silicone\"]").hover();
            $x("//img[@alt=\"Huawei Watch Fit dây silicone\"]").click();
            $x("//a[@href=\"javascript:void(0)\" and @class=\"btn-detail jsArticle\"]").hover();
            $x("//a[@href=\"javascript:void(0)\" and @class=\"btn-detail jsArticle\"]").click();
            $x("//button[@name=\"rating-bad\"]").hover();
            $x("//button[@name=\"rating-bad\"]").click();// K hữu ích
            //$x("//textarea[@class=\"article-rating__reason__input\"]").sendKeys("Sản phẩm tốt");
            $x("//div[@class=\"article-rating__reason__dropdown dropdown-age\"]").click();//độ tuổi
            $x("//div[@class=\"article-rating__reason__dropdown__data\"]//p[text()=\"18-24\"]").click();//18-24
            $x("//div[@class=\"article-rating__reason__dropdown dropdown-gender\"]").click();//giới tính
            $x("//div[@class=\"article-rating__reason__dropdown__data\"]//p[@data-value=\"1\"]").click();//nam
            $x("//button[@name=\"main-button\"]").click();//submit
            //$x("//div[@class=\"btn-closemenu close-tab\"]").click();//dóng
            error=$x("//label[@id=\"inputContent-error\"]").getText();
            if (error != ""){
                resultsTest.put("23", new Object[]{22d, "Test Đánh Giá bài viết SmartWatch Comment: Để trống", "1- Vào trang https://www.thegioididong.com/\n2- Tìm kiếm Đồng Hồ Thông Minh.\n3- Chọn Sản Phẩm\n4- Click vào Button Xem thêm.\n 5- Đánh giá Không hữu ích", "Từ khóa tìm kiếm: Đồng Hồ Thông Minh\nĐánh giá Không Hài Lòng", "Hiển thị kết quả","Thông báo:" +error, dtf.format(now), "Pass",});
            }
        } catch (AssertionError e) {
            resultsTest.put("23", new Object[]{22d, "Test Đánh Giá bài viết SmartWatch Comment: Để trống", "1- Vào trang https://www.thegioididong.com/\n2- Tìm kiếm Đồng Hồ Thông Minh.\n3- Chọn Sản Phẩm\n4- Click vào Button Xem thêm.\n 5- Đánh giá Không hữu ích", "Từ khóa tìm kiếm: Đồng Hồ Thông Minh\nĐánh giá Không Hài Lòng", "Hiển thị kết quả","Thông báo:" +error, dtf.format(now), "Fail",});
            Assert.assertTrue(false);
        }

    }

    @Test(priority = 24)
    public void Ratesmartwatchbadnoagenogender() {
        error="";
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@id=\"skw\"]").sendKeys("Đồng hồ thông minh");
            $x("//button[@type=\"submit\"]").click();
            $x("//div[@class=\"view-more \"]//a").hover();
            $x("//div[@class=\"view-more \"]//a").click();
            $x("//img[@alt=\"Apple Watch S3 GPS 38mm viền nhôm dây cao su đen\"]").hover();
            $x("//img[@alt=\"Apple Watch S3 GPS 38mm viền nhôm dây cao su đen\"]").click();
            $x("//a[@href=\"javascript:void(0)\" and @class=\"btn-detail jsArticle\"]").hover();
            $x("//a[@href=\"javascript:void(0)\" and @class=\"btn-detail jsArticle\"]").click();
            $x("//button[@name=\"rating-bad\"]").hover();
            $x("//button[@name=\"rating-bad\"]").click();//k hữu ích
            $x("//textarea[@class=\"article-rating__reason__input\"]").sendKeys("Sản phẩm xấu");
            //$x("//div[@class=\"article-rating__reason__dropdown dropdown-age\"]").click();//độ tuổi
            // $x("//div[@class=\"article-rating__reason__dropdown__data\"]//p[text()=\"25-34\"]").click();//25-34
            // $x("//div[@class=\"article-rating__reason__dropdown dropdown-gender\"]").click();//giới tính
            // $x("//div[@class=\"article-rating__reason__dropdown__data\"]//p[@data-value=\"2\"]").click();//nam
            $x("//button[@name=\"main-button\"]").click();//submit
            //$x("//div[@class=\"btn-closemenu close-tab\"]").click();//dóng
            error=$x("//div[@class=\"article-rating__thank__left\"]//b[text()='Cảm ơn về thông tin bạn đã chia sẻ.']").getText();
            if (error != ""){
                resultsTest.put("24", new Object[]{23d, "Test Đánh Giá bài viết SmartWatch Tuổi,Giới Tính Để trống", "1- Vào trang https://www.thegioididong.com/\n2- Tìm kiếm Đồng Hồ Thông Minh.\n3- Chọn Sản Phẩm\n4- Click vào Button Xem thêm.\n 5- Đánh giá không hữu ích", "Từ khóa tìm kiếm: Đồng Hồ Thông Minh\nĐánh giá Không Hữu Ích\nGóp ý:Sản Phẩm Xấu\n Độ tuổi:25-34\n Giới tính: Nữ", "Hiển thị kết quả đánh giá","Thông báo:" +error, dtf.format(now), "Pass",});
            }
        } catch (AssertionError e) {
            resultsTest.put("24", new Object[]{23d, "Test Đánh Giá bài viết SmartWatch Tuổi,Giới Tính Để trống", "1- Vào trang https://www.thegioididong.com/\n2- Tìm kiếm Đồng Hồ Thông Minh.\n3- Chọn Sản Phẩm\n4- Click vào Button Xem thêm.\n 5- Đánh giá không hữu ích", "Từ khóa tìm kiếm: Đồng Hồ Thông Minh\nĐánh giá Không Hữu Ích\nGóp ý:Sản Phẩm Xấu\n Độ tuổi:25-34\n Giới tính: Nữ", "Hiển thị kết quả đánh giá","Thông báo:" +error, dtf.format(now), "Fail",});
            Assert.assertTrue(false);
        }
    }

    @Test(priority = 25)
    public void Ratesmartwatchbad() {
        error="";
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@id=\"skw\"]").sendKeys("Đồng hồ thông minh");
            $x("//button[@type=\"submit\"]").click();
            $x("//div[@class=\"view-more \"]//a").hover();
            $x("//div[@class=\"view-more \"]//a").click();
            $x("//img[@alt=\"Apple Watch SE 40mm viền nhôm dây cao su\"]").hover();
            $x("//img[@alt=\"Apple Watch SE 40mm viền nhôm dây cao su\"]").click();
            $x("//a[@href=\"javascript:void(0)\" and @class=\"btn-detail jsArticle\"]").hover();
            $x("//a[@href=\"javascript:void(0)\" and @class=\"btn-detail jsArticle\"]").click();
            $x("//button[@name=\"rating-bad\"]").hover();
            $x("//button[@name=\"rating-bad\"]").click();//k hữu ích
            $x("//textarea[@class=\"article-rating__reason__input\"]").sendKeys("Sản phẩm không tốt");
            $x("//div[@class=\"article-rating__reason__dropdown dropdown-age\"]").click();//độ tuổi
            $x("//div[@class=\"article-rating__reason__dropdown__data\"]//p[text()=\"25-34\"]").click();//25-34
            $x("//div[@class=\"article-rating__reason__dropdown dropdown-gender\"]").click();//giới tính
            $x("//div[@class=\"article-rating__reason__dropdown__data\"]//p[@data-value=\"2\"]").click();//nữ
            $x("//button[@name=\"main-button\"]").click();//submit
            //$x("//div[@class=\"btn-closemenu close-tab\"]").click();//dóng
            error=$x("//label[@id=\"inputContent-error\"]").getText();
            if (error != ""){
                resultsTest.put("25", new Object[]{24d, "Test Đánh Giá bài viết SmartWatch Comment quá Kí tự", "1- Vào trang https://www.thegioididong.com/\n2- Tìm kiếm Đồng Hồ Thông Minh.\n3- Chọn Sản Phẩm\n4- Click vào Button Xem thêm.\n 5- Đánh giá không hữu ích", "Từ khóa tìm kiếm: Đồng Hồ Thông Minh\nĐánh giá Không Hữu Ích\nGóp ý:Sản Phẩm Không Tốt\n Độ tuổi:25-34\n Giới tính: Nữ", "Hiển thị kết quả đánh giá","Thông báo: " +error, dtf.format(now), "Pass",});
            }
        } catch (AssertionError e) {
            resultsTest.put("25", new Object[]{24d, "Test Đánh Giá bài viết SmartWatch Comment quá kí tự", "1- Vào trang https://www.thegioididong.com/\n2- Tìm kiếm Đồng Hồ Thông Minh.\n3- Chọn Sản Phẩm\n4- Click vào Button Xem thêm.\n 5- Đánh giá không hữu ích", "Từ khóa tìm kiếm: Đồng Hồ Thông Minh\nĐánh giá Không Hữu Ích\nGóp ý:Sản Phẩm Không Tốt\n Độ tuổi:25-34\n Giới tính: Nữ", "Hiển thị kết quả đánh giá","Thông báo: " +error, dtf.format(now), "Fail",});
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 26)
//tìm kiếm theo thương hiệu
    public void searchBrand(){
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@id=\"skw\"]").sendKeys("samsung");
            $x("/html/body/header/div[1]/div/form/button/i").click();

            resultsTest.put("26", new Object[]{
                    25d,
                    "Test trường hợp tìm kiếm theo thương hiệu",
                    "1- Vào trang https://www.thegioididong.com/\n2- click Text input search\n3- nhập samsung\n4- click button icon tìm kiếm ",
                    "từ khóa tìm kiếm:samsung",
                    "danh sách sản phẩm thương hiệu samsung",
                    "danh sách sản phẩm thương hiệu samsung",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("26", new Object[]{
                    25d,
                    "Test trường hợp tìm kiếm theo thương hiệu",
                    "1- Vào trang https://www.thegioididong.com/\n2- click Text input search\n3- nhập samsung\n4- click button icon tìm kiếm ",
                    "từ khóa tìm kiếm:samsung",
                    "danh sách sản phẩm thương hiệu samsung",
                    "danh sách sản phẩm thương hiệu khác samsung",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 27)
    //tìm kiếm theo tên đt
    public void searchPhoneName(){


        try{
            open("https://www.thegioididong.com/");
            $x("//*[@id=\"skw\"]").sendKeys("iphone 12 pro max");
            $x("/html/body/header/div[1]/div/form/button/i").click();
            resultsTest.put("27", new Object[]{
                    26d,
                    "Test trường hợp tìm kiếm theo model điện thoại",
                    "1- Vào trang https://www.thegioididong.com/\n2- click Text input search\n3- nhập iphone 12 pro max\n4- click button icon tìm kiếm ",
                    "từ khóa tìm kiếm:iphone 12 pro max",
                    "danh sách sản phẩm iphone 12 pro max",
                    "danh sách sản phẩm iphone 12 pro max",
                    dtf.format(now),
                    "Pass",
            });

        }catch (AssertionError e) {
            resultsTest.put("27", new Object[]{
                    26d,
                    "Test trường hợp tìm kiếm theo model điện thoại",
                    "1- Vào trang https://www.thegioididong.com/\n2- click Text input search\n3- nhập iphone 12 pro max\n4- click button icon tìm kiếm ",
                    "từ khóa tìm kiếm:iphone 12 pro max",
                    "danh sách sản phẩm iphone 12 pro max",
                    "danh sách sản phẩm khác iphone 12 pro max",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 28)
    //tìm kiếm theo chip xử lý
    public void searchChip(){
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@id=\"skw\"]").sendKeys("helio g95");
            $x("/html/body/header/div[1]/div/form/button/i").click();
            resultsTest.put("28", new Object[]{
                    27d,
                    "Test trường hợp tìm kiếm theo chip",
                    "1- Vào trang https://www.thegioididong.com/\n2- click Text input search\n3- nhập helio g95\n4- click button icon tìm kiếm ",
                    "từ khóa tìm kiếm:helio g95",
                    "danh sách sản phẩm có chip xử lý là helio g95",
                    "danh sách sản phẩm có chip xử lý là helio g95",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("28", new Object[]{
                    27d,
                    "Test trường hợp tìm kiếm theo chip",
                    "1- Vào trang https://www.thegioididong.com/\n2- click Text input search\n3- nhập helio g95\n4- click button icon tìm kiếm ",
                    "từ khóa tìm kiếm:helio g95",
                    "danh sách sản phẩm có chip xử lý là helio g95",
                    "danh sách sản phẩm có chip xử lý khác helio g95",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 29)
    //tìm kiếm theo độ phân giải camera
    public void searchCameraMP(){
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@id=\"skw\"]").sendKeys("64 MP");
            $x("/html/body/header/div[1]/div/form/button/i").click();
            resultsTest.put("29", new Object[]{
                    28d,
                    "Test trường hợp tìm kiếm theo độ phân giải camera",
                    "1- Vào trang https://www.thegioididong.com/\n2- click Text input search\n3- nhập 64 MP\n4- click button icon tìm kiếm ",
                    "từ khóa tìm kiếm:64 MP",
                    "danh sách sản phẩm có camera có độ phân giải 64 MP",
                    "danh sách sản phẩm có camera có độ phân giải 64 MP",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("29", new Object[]{
                    28d,
                    "Test trường hợp tìm kiếm theo độ phân giải camera",
                    "1- Vào trang https://www.thegioididong.com/\n2- click Text input search\n3- nhập 64 MP\n4- click button icon tìm kiếm ",
                    "từ khóa tìm kiếm:64 MP",
                    "danh sách sản phẩm có camera có độ phân giải 64 MP",
                    "danh sách sản phẩm có camera có độ phân giải khác 64 MP",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 30)
    //tìm kiếm với tên sai
    public void searchNameFail(){
        try {
            open("https://www.thegioididong.com/");
            $x("//*[@id=\"skw\"]").sendKeys("Xiaomy");
            $x("/html/body/header/div[1]/div/form/button/i").click();
            String x = $x("/html/body/section/div[2]/h3").getText();
            Assert.assertEquals(x, "Để tìm được kết quả chính xác hơn, bạn vui lòng:");
            resultsTest.put("30", new Object[]{
                    29d,
                    "Test trường hợp không tìm thấy sản phẩm",
                    "1- Vào trang https://www.thegioididong.com/\n2- click Text input search\n3- nhập Xiaomy\n4- click button icon tìm kiếm\n5- chuyển đến trang https://www.thegioididong.com/tim-kiem?key=Xiaomy\n6- hiển thị dòng text Để tìm được kết quả chính xác hơn, bạn vui lòng:  ",
                    "từ khóa tìm kiếm:Xiaomy",
                    "không tìm thấy sản phẩm",
                    "không tìm thấy sản phẩm",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("30", new Object[]{
                    29d,
                    "Test trường hợp không tìm thấy sản phẩm",
                    "1- Vào trang https://www.thegioididong.com/\n2- click Text input search\n3- nhập Xiaomy\n4- click button icon tìm kiếm\n5- chuyển đến trang https://www.thegioididong.com/tim-kiem?key=Xiaomy\n6- hiển thị dòng text Để tìm được kết quả chính xác hơn, bạn vui lòng: ",
                    "từ khóa tìm kiếm:Xiaomy",
                    "không tìm thấy sản phẩm",
                    "tìm thấy sản phẩm",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 31)
    public void seePhone(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            //$x("/html/body/div[8]/section/div[1]/div/div[4]/div[1]").click();
            resultsTest.put("31", new Object[]{
                    30d,
                    "Test xem điện thoại",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại ",
                    "điện thoại",
                    "danh sách sản phẩm điện thoại",
                    "danh sách sản phẩm điện thoại",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("31", new Object[]{
                    30d,
                    "Test xem điện thoại",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại ",
                    "điện thoại",
                    "danh sách sản phẩm điện thoại",
                    "danh sách sản phẩm khác điện thoại",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 32)
    public void fillPhoneBrand(){
        try {
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[1]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[2]/div[1]/a[5]/img").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[2]/div[2]/a[2]").click();
            resultsTest.put("32", new Object[]{
                    31d,
                    "Test lọc điện thoại theo thương hiệu",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button hãng\n4- click vào button img xiaomi\n5- click vào button xem kết quả ",
                    "thương hiệu:xiaomy",
                    "danh sách sản phẩm điện thoại của thương hiệu xiaomy",
                    "danh sách sản phẩm điện thoại của thương hiệu xiaomy",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("32", new Object[]{
                    31d,
                    "Test lọc điện thoại theo thương hiệu",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button hãng\n4- click vào button img xiaomi\n5- click vào button xem kết quả ",
                    "thương hiệu:xiaomy",
                    "danh sách sản phẩm điện thoại của thương hiệu xiaomy",
                    "danh sách sản phẩm điện thoại khác thương hiệu xiaomy",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 33)
    public void fillPhonePrice(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div[1]/div/div[4]/div[1]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[4]/div[2]/div[1]/a[3]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[4]/div[2]/div[3]/a[2]").click();
            resultsTest.put("33", new Object[]{
                    32d,
                    "Test lọc điện thoại theo giá",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button giá\n4- click vào button Từ 4 - 7 triệu\n5- click vào button xem kết quả ",
                    "giá:từ 4-7 triệu",
                    "danh sách sản phẩm điện thoại có giá từ 4-7 triệu",
                    "danh sách sản phẩm điện thoại có giá từ 4-7 triệu",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("33", new Object[]{
                    32d,
                    "Test lọc điện thoại theo giá",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button giá\n4- click vào button Từ 4 - 7 triệu\n5- click vào button xem kết quả ",
                    "giá:từ 4-7 triệu",
                    "danh sách sản phẩm điện thoại có giá từ 4-7 triệu",
                    "danh sách sản phẩm điện thoại có giá khác từ 4-7 triệu",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 34)
    public void fillPhoneSpecies(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div[1]/div/div[5]/div[1]/span").click();
            $x("/html/body/div[8]/section/div[1]/div/div[5]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[5]/div[2]/div[2]/a[2]").click();
            resultsTest.put("34", new Object[]{
                    33d,
                    "Test lọc điện thoại theo loại",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Loại điện thoại\n4- click vào button Android\n5- click vào button xem kết quả ",
                    "loại điện thoại:Android",
                    "danh sách sản phẩm điện thoại Android",
                    "danh sách sản phẩm điện thoại Android",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("34", new Object[]{
                    33d,
                    "Test lọc điện thoại theo loại",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Loại điện thoại\n4- click vào button Android\n5- click vào button xem kết quả ",
                    "loại điện thoại:Android",
                    "danh sách sản phẩm điện thoại Android",
                    "danh sách sản phẩm điện thoại khác Android",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 35)
    public void fillPhonePerformance(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div[1]/div/div[6]/div[1]/span").click();
            $x("/html/body/div[8]/section/div[1]/div/div[6]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[6]/div[2]/div[2]/a[2]").click();
            resultsTest.put("35", new Object[]{
                    34d,
                    "Test lọc điện thoại theo Pin và hiệu năng",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Pin và hiệu năng\n4- click vào button Chơi game/Cấu hình cao\n5- click vào button xem kết quả ",
                    "Pin và hiệu năng:chơi game/ cấu hình khủng",
                    "danh sách sản phẩm điện thoại có cấu hình khủng",
                    "danh sách sản phẩm điện thoại có cấu hình khủng",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("35", new Object[]{
                    34d,
                    "Test lọc điện thoại theo loại",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Pin và hiệu năng\n4- click vào button Chơi game/Cấu hình cao\n5- click vào button xem kết quả ",
                    "Pin và hiệu năng:chơi game/ cấu hình cao",
                    "danh sách sản phẩm điện thoại có cấu hình cao",
                    "danh sách sản phẩm điện thoại không có cấu hình cao",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 36)
    public void fillPhoneRam(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[2]/div[1]/a[5]").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[2]/div[2]/a[2]").click();
            resultsTest.put("36", new Object[]{
                    35d,
                    "Test lọc điện thoại theo dung lượng ram",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button RAM\n4- click vào button 6 GB\n5- click vào button xem kết quả ",
                    "ram: 6 gb",
                    "danh sách sản phẩm điện thoại có ram 6gb",
                    "danh sách sản phẩm điện thoại có ram 6gb",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("36", new Object[]{
                    35d,
                    "Test lọc điện thoại theo dung lượng ram",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button RAM\n4- click vào button 6 GB\n5- click vào button xem kết quả ",
                    "ram: 6 gb",
                    "danh sách sản phẩm điện thoại có ram 6gb",
                    "danh sách sản phẩm điện thoại có ram khác 6gb",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 37)
    public void fillPhoneRom(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[2]/div[1]/a[4]").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[2]/div[2]/a[2]").click();
            resultsTest.put("37", new Object[]{
                    36d,
                    "Test lọc điện thoại theo bộ nhớ trong",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Bộ nhớ trong\n4- click vào button 64 GB\n5- click vào button xem kết quả ",
                    "bộ nhớ trong: 64 gb",
                    "danh sách sản phẩm điện thoại có bộ nhớ trong là 64 gb",
                    "danh sách sản phẩm điện thoại có bộ nhớ trong là 64 gb",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("37", new Object[]{
                    36d,
                    "Test lọc điện thoại theo bộ nhớ trong",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Bộ nhớ trong\n4- click vào button 64 GB\n5- click vào button xem kết quả ",
                    "bộ nhớ trong: 64 gb",
                    "danh sách sản phẩm điện thoại có bộ nhớ trong là 64 gb",
                    "danh sách sản phẩm điện thoại có bộ nhớ trong khác 64 gb",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 38)
    public void fillPhoneCamera(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div/div/div[9]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[9]/div[2]/div[1]/a[2]").click();
            $x("/html/body/div[8]/section/div/div/div[9]/div[2]/div[2]/a[2]").click();
            resultsTest.put("38", new Object[]{
                    37d,
                    "Test lọc điện thoại theo camera",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Camera\n4- click vào button Chụp góc rộng\n5- click vào button xem kết quả ",
                    "camera: chụp góc rộng",
                    "danh sách sản phẩm điện thoại có chức năng chụp góc rộng",
                    "danh sách sản phẩm điện thoại có chức năng chụp góc rộng",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("38", new Object[]{
                    37d,
                    "Test lọc điện thoại theo camera",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Camera\n4- click vào button Chụp góc rộng\n5- click vào button xem kết quả ",
                    "camera: chụp góc rộng",
                    "danh sách sản phẩm điện thoại có chức năng chụp góc rộng",
                    "danh sách sản phẩm điện thoại không có chức năng chụp góc rộng",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 39)
    public void fillPhoneSpecialFeature(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div/div/div[10]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[10]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[10]/div[2]/div[2]/a[2]").click();
            resultsTest.put("39", new Object[]{
                    38d,
                    "Test lọc điện thoại theo Tính năng đặt biệt",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Tính năng đặt biệt\n4- click vào button Hỗ trợ 5G\n5- click vào button xem kết quả ",
                    "tính năng đặt biệt: hỗ trợ 5G",
                    "danh sách sản phẩm điện thoại có hỗ trợ 5G",
                    "danh sách sản phẩm điện thoại có hỗ trợ 5G",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("39", new Object[]{
                    38d,
                    "Test lọc điện thoại theo Tính năng đặt biệt",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Tính năng đặt biệt\n4- click vào button Hỗ trợ 5G\n5- click vào button xem kết quả ",
                    "tính năng đặt biệt: hỗ trợ 5G",
                    "danh sách sản phẩm điện thoại có hỗ trợ 5G",
                    "danh sách sản phẩm điện thoại không có hỗ trợ 5G",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 40)
    public void fillPhoneDesign(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div/div/div[11]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[11]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[11]/div[2]/div[2]/a[2]").click();
            resultsTest.put("40", new Object[]{
                    39d,
                    "Test lọc điện thoại theo Thiết kế",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Thiết kế\n4- click vào button Tràn viền\n5- click vào button xem kết quả ",
                    "thiết kế: tràn viền",
                    "danh sách sản phẩm điện thoại có thiết kế tràn viền",
                    "danh sách sản phẩm điện thoại có thiết kế tràn viền",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("40", new Object[]{
                    39d,
                    "Test lọc điện thoại theo Thiết kế",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Thiết kế\n4- click vào button Tràn viền\n5- click vào button xem kết quả ",
                    "thiết kế: tràn viền",
                    "danh sách sản phẩm điện thoại có thiết kế tràn viền",
                    "danh sách sản phẩm điện thoại không có thiết kế tràn viền",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 41)
    public void fillPhoneCreen(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div/div/div[12]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[12]/div[2]/div[1]/a[2]").click();
            $x("/html/body/div[8]/section/div/div/div[12]/div[2]/div[2]/a[2]").click();
            resultsTest.put("41", new Object[]{
                    40d,
                    "Test lọc điện thoại theo màn hình",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Màn hình\n4- click vào button Từ 6 inch trở lên\n5- click vào button xem kết quả ",
                    "màn hình: từ 6 inch trở lên",
                    "danh sách sản phẩm điện thoại có màn hình từ 6 inch trở lên",
                    "danh sách sản phẩm điện thoại có màn hình từ 6 inch trở lên",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("41", new Object[]{
                    40d,
                    "Test lọc điện thoại theo màn hình",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Màn hình\n4- click vào button Từ 6 inch trở lên\n5- click vào button xem kết quả ",
                    "màn hình: từ 6 inch trở lên",
                    "danh sách sản phẩm điện thoại có màn hình từ 6 inch trở lên",
                    "danh sách sản phẩm điện thoại không có màn hình từ 6 inch trở lên",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 42)
    public void fillPhoneRamAndRom(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[2]/div[1]/a[5]").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[2]/div[1]/a[4]").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[2]/div[2]/a[2]").click();
            resultsTest.put("42", new Object[]{
                    41d,
                    "Test lọc điện thoại theo 2 thuộc tính",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Ram\n4- click vào button 6 GB\n5- click vào button Bộ nhớ trong\n6- click vào button 64 GB\n 7- click vào button xem kết quả ",
                    "ram: 6g\n bộ nhớ trong: 64g",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("42", new Object[]{
                    41d,
                    "Test lọc điện thoại theo 2 thuộc tính",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Ram\n4- click vào button 6 GB\n5- click vào button Bộ nhớ trong\n6- click vào button 64 GB\n 7- click vào button xem kết quả ",
                    "ram: 6g\n bộ nhớ trong: 64g",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    "danh sách sản phẩm điện thoại không có những thuộc tính đã chọn",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 43)
    public void fillPhoneBrandRamRom(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[1]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[2]/div[1]/a[5]/img").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[2]/div[1]/a[5]").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[2]/div[1]/a[4]").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[2]/div[2]/a[2]").click();
            resultsTest.put("43", new Object[]{
                    42d,
                    "Test lọc điện thoại theo 3 thuộc tính",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n 3- click vào button Hãng\n4- click vào button xiaomi\n5- click vào button Ram\n6- click vào button 6 GB\n7- click vào button Bộ nhớ trong\n8- click vào button 64 GB\n 9- click vào button xem kết quả ",
                    "Hãng: xiaomi\nRam: 6 GB\nBộ nhớ trong: 64 GB",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("43", new Object[]{
                    42d,
                    "Test lọc điện thoại theo 3 thuộc tính",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n 3- click vào button Hãng\n4- click vào button xiaomi\n5- click vào button Ram\n6- click vào button 6 GB\n7- click vào button Bộ nhớ trong\n8- click vào button 64 GB\n 9- click vào button xem kết quả ",
                    "Hãng: xiaomi\nRam: 6 GB\nBộ nhớ trong: 64 GB",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    "danh sách sản phẩm điện thoại không có những thuộc tính đã chọn",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 44)
    public void fillPhoneBrandPerformanceRamRom(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[1]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[2]/div[1]/a[5]/img").click();
            $x("/html/body/div[8]/section/div[1]/div/div[6]/div[1]/span").click();
            $x("/html/body/div[8]/section/div[1]/div/div[6]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[2]/div[1]/a[6]").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[2]/div[1]/a[5]").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[2]/div[2]/a[2]").click();
            resultsTest.put("44", new Object[]{
                    43d,
                    "Test lọc điện thoại theo 4 thuộc tính",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Pin và hiệu năng\n4- click vào button Chơi game/Cấu hình cao\n5- click vào button Hãng\n6- click vào button xiaomi\n7- click vào button Ram\n8- click vào button 6 GB\n9- click vào button Bộ nhớ trong\n10- click vào button 128 GB\n11- click vào button xem kết quả ",
                    "Hãng: xiaomi\nRam: 6 GB\nBộ nhớ trong: 128 GB\nPin và hệu năng: Chơi game/cấu hình cao",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("44", new Object[]{
                    43d,
                    "Test lọc điện thoại theo 4 thuộc tính",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Pin và hiệu năng\n4- click vào button Chơi game/Cấu hình cao\n5- click vào button Hãng\n6- click vào button xiaomi\n7- click vào button Ram\n8- click vào button 6 GB\n9- click vào button Bộ nhớ trong\n10- click vào button 128 GB\n11- click vào button xem kết quả ",
                    "Hãng: xiaomi\nRam: 6 GB\nBộ nhớ trong: 128 GB\nPin và hệu năng: Chơi game/cấu hình cao",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    "danh sách sản phẩm điện thoại không có những thuộc tính đã chọn",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 45)
    public void fillPhoneBrandPerformanceRamRomDesign(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[1]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[2]/div[1]/a[5]/img").click();
            $x("/html/body/div[8]/section/div[1]/div/div[6]/div[1]/span").click();
            $x("/html/body/div[8]/section/div[1]/div/div[6]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[2]/div[1]/a[6]").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[2]/div[1]/a[5]").click();
            $x("/html/body/div[8]/section/div/div/div[11]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[11]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[11]/div[2]/div[2]/a[2]").click();
            resultsTest.put("45", new Object[]{
                    44d,
                    "Test lọc điện thoại theo 5 thuộc tính",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Pin và hiệu năng\n4- click vào button Chơi game/Cấu hình cao\n5- click vào button Hãng\n6- click vào button xiaomi\n7- click vào button Ram\n8- click vào button 6 GB\n9- click vào button Bộ nhớ trong\n10- click vào button 128 GB\n11- click vào button Thiết kế\n12- click vào button Tràn viền\n13- click vào button xem kết quả ",
                    "Hãng: xiaomi\nRam: 6 GB\nBộ nhớ trong: 128 GB\nPin và hệu năng: Chơi game/cấu hình cao\n thiết kế: tràn viền",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("45", new Object[]{
                    44d,
                    "Test lọc điện thoại theo 5 thuộc tính",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Pin và hiệu năng\n4- click vào button Chơi game/Cấu hình cao\n5- click vào button Hãng\n6- click vào button xiaomi\n7- click vào button Ram\n8- click vào button 6 GB\n9- click vào button Bộ nhớ trong\n10- click vào button 128 GB\n11- click vào button Thiết kế\n12- click vào button Tràn viền\n13- click vào button xem kết quả ",
                    "Hãng: xiaomi\nRam: 6 GB\nBộ nhớ trong: 128 GB\nPin và hệu năng: Chơi game/cấu hình cao\n thiết kế: tràn viền",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    "danh sách sản phẩm điện thoại không có những thuộc tính đã chọn",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 46)
    public void fillPhoneBrandSpeciesPerformanceRamRomDesign(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[1]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[2]/div[1]/a[5]/img").click();
            $x("/html/body/div[8]/section/div[1]/div/div[6]/div[1]/span").click();
            $x("/html/body/div[8]/section/div[1]/div/div[6]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[2]/div[1]/a[6]").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[2]/div[1]/a[5]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[5]/div[1]/span").click();
            $x("/html/body/div[8]/section/div[1]/div/div[5]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[11]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[11]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[11]/div[2]/div[2]/a[2]").click();
            resultsTest.put("46", new Object[]{
                    45d,
                    "Test lọc điện thoại theo 6 thuộc tính",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Pin và hiệu năng\n4- click vào button Chơi game/Cấu hình cao\n5- click vào button Hãng\n6- click vào button xiaomi\n7- click vào button Ram\n8- click vào button 6 GB\n9- click vào button Bộ nhớ trong\n10- click vào button 128 GB\n11-click vào button Loại điện thoại\n12- click vào button Android\n13- click vào button Thiết kế\n14- click vào button Tràn viền\n15- click vào button xem kết quả ",
                    "Hãng: xiaomi\nRam: 6 GB\nBộ nhớ trong: 128 GB\nPin và hệu năng: Chơi game/cấu hình cao\n loại điện thoại:Android\n thiết kế: tràn viền",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("46", new Object[]{
                    45d,
                    "Test lọc điện thoại theo 6 thuộc tính",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Pin và hiệu năng\n4- click vào button Chơi game/Cấu hình cao\n5- click vào button Hãng\n6- click vào button xiaomi\n7- click vào button Ram\n8- click vào button 6 GB\n9- click vào button Bộ nhớ trong\n10- click vào button 128 GB\n11-click vào button Loại điện thoại\n12- click vào button Android\n13- click vào button Thiết kế\n14- click vào button Tràn viền\n15- click vào button xem kết quả ",
                    "Hãng: xiaomi\nRam: 6 GB\nBộ nhớ trong: 128 GB\nPin và hệu năng: Chơi game/cấu hình cao\n loại điện thoại:Android\n thiết kế: tràn viền",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    "danh sách sản phẩm điện thoại không có những thuộc tính đã chọn",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 47)
    public void fillPhoneBrandPriceSpeciesPerformanceRamRomDesign(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[1]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[2]/div[1]/a[5]/img").click();
            $x("/html/body/div[8]/section/div[1]/div/div[6]/div[1]/span").click();
            $x("/html/body/div[8]/section/div[1]/div/div[6]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[2]/div[1]/a[6]").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[2]/div[1]/a[5]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[5]/div[1]/span").click();
            $x("/html/body/div[8]/section/div[1]/div/div[5]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[4]/div[1]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[4]/div[2]/div[1]/a[4]").click();
            $x("/html/body/div[8]/section/div/div/div[11]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[11]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[11]/div[2]/div[2]/a[2]").click();
            resultsTest.put("47", new Object[]{
                    46d,
                    "Test lọc điện thoại theo 7 thuộc tính",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Pin và hiệu năng\n4- click vào button Chơi game/Cấu hình cao\n5- click vào button Hãng\n6- click vào button xiaomi\n7- click vào button Ram\n8- click vào button 6 GB\n9- click vào button Bộ nhớ trong\n10- click vào button 128 GB\n11-click vào button Loại điện thoại\n12- click vào button Android\n13- click vào button Giá\n14- click vào button Từ 7 - 13 triệu\n15- click vào button Thiết kế\n16- click vào button Tràn viền\n17- click vào button xem kết quả ",
                    "Hãng: xiaomi\nRam: 6 GB\nBộ nhớ trong: 128 GB\nPin và hệu năng: Chơi game/cấu hình cao\ngiá: Từ 7 - 13 triệu\n loại điện thoại:Android\n thiết kế: tràn viền",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("47", new Object[]{
                    46d,
                    "Test lọc điện thoại theo 7 thuộc tính",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Pin và hiệu năng\n4- click vào button Chơi game/Cấu hình cao\n5- click vào button Hãng\n6- click vào button xiaomi\n7- click vào button Ram\n8- click vào button 6 GB\n9- click vào button Bộ nhớ trong\n10- click vào button 128 GB\n11-click vào button Loại điện thoại\n12- click vào button Android\n13- click vào button Giá\n14- click vào button Từ 7 - 13 triệu\n15- click vào button Thiết kế\n16- click vào button Tràn viền\n17- click vào button xem kết quả ",
                    "Hãng: xiaomi\nRam: 6 GB\nBộ nhớ trong: 128 GB\nPin và hệu năng: Chơi game/cấu hình cao\ngiá: Từ 7 - 13 triệu\n loại điện thoại:Android\n thiết kế: tràn viền",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    "danh sách sản phẩm điện thoại không có những thuộc tính đã chọn",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 48)
    public void fillPhoneBrandPriceSpeciesPerformanceRamRomCamereDesign(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[1]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[2]/div[1]/a[5]/img").click();
            $x("/html/body/div[8]/section/div[1]/div/div[6]/div[1]/span").click();
            $x("/html/body/div[8]/section/div[1]/div/div[6]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[2]/div[1]/a[6]").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[2]/div[1]/a[5]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[4]/div[1]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[4]/div[2]/div[1]/a[4]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[5]/div[1]/span").click();
            $x("/html/body/div[8]/section/div[1]/div/div[5]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[11]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[11]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[9]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[9]/div[2]/div[1]/a[2]").click();
            $x("/html/body/div[8]/section/div/div/div[9]/div[2]/div[2]/a[2]").click();
            resultsTest.put("48", new Object[]{
                    47d,
                    "Test lọc điện thoại theo 8 thuộc tính",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Pin và hiệu năng\n4- click vào button Chơi game/Cấu hình cao\n5- click vào button Hãng\n6- click vào button xiaomi\n7- click vào button Ram\n8- click vào button 6 GB\n9- click vào button Bộ nhớ trong\n10- click vào button 128 GB\n11-click vào button Loại điện thoại\n12- click vào button Android\n13- click vào button Giá\n14- click vào button Từ 7 - 13 triệu\n15- click vào button Thiết kế\n16- click vào button Tràn viền\n17- click vào button Camera\n18- click vào button Chụp góc rộng\n19- click vào button xem kết quả ",
                    "Hãng: xiaomi\nRam: 6 GB\nBộ nhớ trong: 128 GB\nPin và hệu năng: Chơi game/cấu hình cao\ngiá: Từ 7 - 13 triệu\n loại điện thoại:Android\n thiết kế: tràn viền\n camera: chụp góc rộng",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("48", new Object[]{
                    47d,
                    "Test lọc điện thoại theo 8 thuộc tính",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Pin và hiệu năng\n4- click vào button Chơi game/Cấu hình cao\n5- click vào button Hãng\n6- click vào button xiaomi\n7- click vào button Ram\n8- click vào button 6 GB\n9- click vào button Bộ nhớ trong\n10- click vào button 128 GB\n11-click vào button Loại điện thoại\n12- click vào button Android\n13- click vào button Giá\n14- click vào button Từ 7 - 13 triệu\n15- click vào button Thiết kế\n16- click vào button Tràn viền\n17- click vào button Camera\n18- click vào button Chụp góc rộng\n19- click vào button xem kết quả ",
                    "Hãng: xiaomi\nRam: 6 GB\nBộ nhớ trong: 128 GB\nPin và hệu năng: Chơi game/cấu hình cao\ngiá: Từ 7 - 13 triệu\n loại điện thoại:Android\n thiết kế: tràn viền\n camera: chụp góc rộng",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    "danh sách sản phẩm điện thoại không có những thuộc tính đã chọn",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 49)
    public void fillPhoneBrandPriceSpeciesPerformanceRamRomCamereDesignCreen(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[1]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[2]/div[1]/a[5]/img").click();
            $x("/html/body/div[8]/section/div[1]/div/div[6]/div[1]/span").click();
            $x("/html/body/div[8]/section/div[1]/div/div[6]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[2]/div[1]/a[6]").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[2]/div[1]/a[5]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[4]/div[1]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[4]/div[2]/div[1]/a[4]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[5]/div[1]/span").click();
            $x("/html/body/div[8]/section/div[1]/div/div[5]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[11]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[11]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[9]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[9]/div[2]/div[1]/a[2]").click();
            $x("/html/body/div[8]/section/div/div/div[12]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[12]/div[2]/div[1]/a[2]").click();
            $x("/html/body/div[8]/section/div/div/div[12]/div[2]/div[2]/a[2]").click();
            resultsTest.put("49", new Object[]{
                    48d,
                    "Test lọc điện thoại theo 9 thuộc tính",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Pin và hiệu năng\n4- click vào button Chơi game/Cấu hình cao\n5- click vào button Hãng\n6- click vào button xiaomi\n7- click vào button Ram\n8- click vào button 6 GB\n9- click vào button Bộ nhớ trong\n10- click vào button 128 GB\n11-click vào button Loại điện thoại\n12- click vào button Android\n13- click vào button Giá\n14- click vào button Từ 7 - 13 triệu\n15- click vào button Thiết kế\n16- click vào button Tràn viền\n17- click vào button Camera\n18- click vào button Chụp góc rộng\n19- click vào button Màn hình\n20- click vào button Từ 6 inch trở lên\n21- click vào button xem kết quả ",
                    "Hãng: xiaomi\nRam: 6 GB\nBộ nhớ trong: 128 GB\nPin và hệu năng: Chơi game/cấu hình cao\ngiá: Từ 7 - 13 triệu\n loại điện thoại:Android\n thiết kế: tràn viền\n camera: chụp góc rộng\n màn hình: từ 6 inch trở lên",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("49", new Object[]{
                    48d,
                    "Test lọc điện thoại theo 9 thuộc tính",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Pin và hiệu năng\n4- click vào button Chơi game/Cấu hình cao\n5- click vào button Hãng\n6- click vào button xiaomi\n7- click vào button Ram\n8- click vào button 6 GB\n9- click vào button Bộ nhớ trong\n10- click vào button 128 GB\n11-click vào button Loại điện thoại\n12- click vào button Android\n13- click vào button Giá\n14- click vào button Từ 7 - 13 triệu\n15- click vào button Thiết kế\n16- click vào button Tràn viền\n17- click vào button Camera\n18- click vào button Chụp góc rộng\n19- click vào button Màn hình\n20- click vào button Từ 6 inch trở lên\n21- click vào button xem kết quả ",
                    "Hãng: xiaomi\nRam: 6 GB\nBộ nhớ trong: 128 GB\nPin và hệu năng: Chơi game/cấu hình cao\ngiá: Từ 7 - 13 triệu\n loại điện thoại:Android\n thiết kế: tràn viền\n camera: chụp góc rộng\n màn hình: từ 6 inch trở lên",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    "danh sách sản phẩm điện thoại không có những thuộc tính đã chọn",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 50)
    public void fillPhoneBrandPriceSpeciesPerformanceRamRomCamereSpecialDesignCreen(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[1]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[3]/div[2]/div[1]/a[5]/img").click();
            $x("/html/body/div[8]/section/div[1]/div/div[6]/div[1]/span").click();
            $x("/html/body/div[8]/section/div[1]/div/div[6]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[2]/div[1]/a[6]").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[2]/div[1]/a[5]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[4]/div[1]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[4]/div[2]/div[1]/a[4]").click();
            $x("/html/body/div[8]/section/div[1]/div/div[5]/div[1]/span").click();
            $x("/html/body/div[8]/section/div[1]/div/div[5]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[11]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[11]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[9]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[9]/div[2]/div[1]/a[2]").click();
            $x("/html/body/div[8]/section/div/div/div[12]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[12]/div[2]/div[1]/a[2]").click();
            $x("/html/body/div[8]/section/div/div/div[10]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[10]/div[2]/div[1]/a[3]").click();
            $x("/html/body/div[8]/section/div/div/div[10]/div[2]/div[2]/a[2]").click();
            resultsTest.put("50", new Object[]{
                    49d,
                    "Test lọc điện thoại theo 10 thuộc tính",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Pin và hiệu năng\n4- click vào button Chơi game/Cấu hình cao\n5- click vào button Hãng\n6- click vào button xiaomi\n7- click vào button Ram\n8- click vào button 6 GB\n9- click vào button Bộ nhớ trong\n10- click vào button 128 GB\n11-click vào button Loại điện thoại\n12- click vào button Android\n13- click vào button Giá\n14- click vào button Từ 7 - 13 triệu\n15- click vào button Thiết kế\n16- click vào button Tràn viền\n17- click vào button Camera\n18- click vào button Chụp góc rộng\n19- click vào button Màn hình\n20- click vào button Từ 6 inch trở lên\n21- click vào Tính năng đặt biệt\n22- click vào Bảo mật vân tay\n23- click vào button xem kết quả ",
                    "Hãng: xiaomi\nRam: 6 GB\nBộ nhớ trong: 128 GB\nPin và hệu năng: Chơi game/cấu hình cao\ngiá: Từ 7 - 13 triệu\n loại điện thoại:Android\n thiết kế: tràn viền\n camera: chụp góc rộng\n màn hình: từ 6 inch trở lên\n tính năng đặt biệt: bảo mật vân tay",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("50", new Object[]{
                    49d,
                    "Test lọc điện thoại theo 10 thuộc tính",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Pin và hiệu năng\n4- click vào button Chơi game/Cấu hình cao\n5- click vào button Hãng\n6- click vào button xiaomi\n7- click vào button Ram\n8- click vào button 6 GB\n9- click vào button Bộ nhớ trong\n10- click vào button 128 GB\n11-click vào button Loại điện thoại\n12- click vào button Android\n13- click vào button Giá\n14- click vào button Từ 7 - 13 triệu\n15- click vào button Thiết kế\n16- click vào button Tràn viền\n17- click vào button Camera\n18- click vào button Chụp góc rộng\n19- click vào button Màn hình\n20- click vào button Từ 6 inch trở lên\n21- click vào Tính năng đặt biệt\n22- click vào Bảo mật vân tay\n23- click vào button xem kết quả ",
                    "Hãng: xiaomi\nRam: 6 GB\nBộ nhớ trong: 128 GB\nPin và hệu năng: Chơi game/cấu hình cao\ngiá: Từ 7 - 13 triệu\n loại điện thoại:Android\n thiết kế: tràn viền\n camera: chụp góc rộng\n màn hình: từ 6 inch trở lên\n tính năng đặt biệt: bảo mật vân tay",
                    "danh sách sản phẩm điện thoại có những thuộc tính đã chọn",
                    "danh sách sản phẩm điện thoại không có những thuộc tính đã chọn",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @Test(priority = 51)
    public void filterCantNotProducts(){
        try{
            open("https://www.thegioididong.com/");
            $x("/html/body/header/div[2]/div/ul/li[1]/a").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[7]/div[2]/div[1]/a[1]").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[1]/span").click();
            $x("/html/body/div[8]/section/div/div/div[8]/div[2]/div[1]/a[4]").click();
            boolean w=$x("/html/body/div[8]/section/div[1]/div/div[8]/div[2]/div[2]/a[2]").isEnabled();
            w=false;
            resultsTest.put("51", new Object[]{
                    50d,
                    "Test lọc điện thoại tìm không có sản phẩm ",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Ram\n4- click vào button 1 GB\n5- click vào button Bộ nhớ trong\n6- click vào button 64 GB\n7- không thể click vào button xem kết quả ",
                    "Ram: 1 GB\nBộ nhớ trong: 64 GB",
                    "button xem kết quả bị vô hiệu hóa",
                    "button xem kết quả bị vô hiệu hóa",
                    dtf.format(now),
                    "Pass",
            });
        }catch (AssertionError e) {
            resultsTest.put("51", new Object[]{
                    50d,
                    "Test lọc điện thoại tìm không có sản phẩm ",
                    "1- Vào trang https://www.thegioididong.com/\n2- click vào button Điện Thoại\n3- click vào button Ram\n4- click vào button 1 GB\n5- click vào button Bộ nhớ trong\n6- click vào button 64 GB\n7- không thể click vào button xem kết quả ",
                    "Ram: 1 GB\nBộ nhớ trong: 64 GB",
                    "button xem kết quả bị vô hiệu hóa",
                    "button xem kết quả không bị vô hiệu hóa",
                    dtf.format(now),
                    "Fail",
            });
            Assert.assertTrue(false);
        }
    }
    @AfterClass
    public void Finish() {
        System.out.println("Kết thúc");
        Set<String> keyset = resultsTest.keySet();
        int rowNum = 0;
        for (String key : keyset) {
            Row row = sheet.createRow(rowNum++);
            Object[] objArr = resultsTest.get(key);
            int cellNum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellNum++);
                if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {
            FileOutputStream out = new FileOutputStream(new File("ResultTestToExCel.xls"));
            workbook.write(out);
            out.close();
            System.out.println("In file excel thành công!");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
